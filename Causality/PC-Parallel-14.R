# This includes the implementations of Parallelized PC
# 
# Author: tao
###############################################################################

library(pcalg)
library(parallel)
#library(snpEnrichment)

pcParallel <- function(suffStat, indepTest, p, alpha, verbose = FALSE, fixedGaps = NULL,
		fixedEdges = NULL, NAdelete = TRUE, m.max = Inf, u2pd = "rand") {
	## Initial Checks
	cl <- match.call()
	
	## Skeleton
	skel <- skeletonParallel(suffStat, indepTest, p, alpha, verbose = verbose,
			fixedGaps = fixedGaps, fixedEdges = fixedEdges,
			NAdelete = NAdelete, m.max = m.max)
	
	## Orient edges
	res <- switch (u2pd, "rand" = udag2pdag(skel),
			"retry" = udag2pdagSpecial(skel)$pcObj,
			"relaxed" = udag2pdagRelaxed(skel))
	
	## Output
	res
}

# using multicore - mclapply()
skeletonParallel <- function(suffStat, indepTest, p, alpha, verbose = FALSE,
		fixedGaps = NULL, fixedEdges = NULL, NAdelete = TRUE,
		m.max = Inf) {
	start<-proc.time()
	cl <- match.call()
	
	pval <- NULL
	NoCITest=0
	
	## start skeleton
	sepset <- vector("list", p)
	n.edgetests <- numeric(1)# final length = max { ord}
	
	## fixed gaps
	if (is.null(fixedGaps)) {
		## G := complete graph :
		G <- matrix(rep(TRUE,p*p), nrow = p, ncol = p)
		diag(G) <- FALSE
	} else {
		if (!(identical(dim(fixedGaps),c(p,p)))) {
			stop("Dimensions of the dataset and fixedGaps do not agree.")
		} else {
			if (fixedGaps != t(fixedGaps)) {
				stop("fixedGaps must be symmetric")
			}
			G <- !fixedGaps
		} ## if (!(identical(dim(fixedGaps),c(p,p))))
	} ## if(is.null(G))
	
	## fixed edges
	if (is.null(fixedEdges)) {
		fixedEdges <- matrix(rep(FALSE,p*p), nrow = p, ncol = p)
	} else {
		if (!(identical(dim(fixedEdges),c(p,p)))) {
			stop("Dimensions of the dataset and fixedEdges do not agree.")
		}
		if (fixedEdges != t(fixedEdges)) {
			stop("fixedEdges must be symmetric")
		}
	} ## if(is.null(fixedEdges))
	
	seq_p <- 1:p
	for (iList in 1:p) sepset[[iList]] <- vector("list",p)
	## save maximal p value
	pMax <- matrix(rep(-Inf,p*p),nrow=p,ncol=p)
	diag(pMax) <- 1
	
	done <- FALSE
	ord <- 0
	
	# define wrapper function for CI tests of each task
	testCI <- function(i) {
		x <- as.integer(ind[i,1])
		y <- as.integer(ind[i,2])
		num_tests <- as.integer(0)
		G_yx <- TRUE
		pMax_yx <- -Inf
		sepset_yx <- NULL
		done_xy <- TRUE
		if (G[y,x] && (!fixedEdges[y,x])) {
			nbrsBool <- Gnbrs[,x]
			nbrsBool[y] <- FALSE
			nbrs <- seq_p[nbrsBool]
			rm(nbrsBool)
			length_nbrs <- length(nbrs)
			if (length_nbrs >= ord) {
				if (length_nbrs > ord) done_xy <- FALSE
				S <- seq(length = ord)
				repeat { ## condition w.r.to all  nbrs[S] of size 'ord'
					num_tests <- num_tests + 1
					pval <- indepTest(x,y, nbrs[S], suffStat) # 0.001
					if (is.na(pval)) pval <- ifelse(NAdelete,1,0)
					if (pval > pMax_yx) pMax_yx <- pval
					if(pval >= alpha) { # independent
						G_yx <- FALSE
						sepset_yx <- nbrs[S]
						break
					} else {
						nextSet <- getNextSet(length_nbrs, ord, S)
						if(nextSet$wasLast)
							break
						S <- nextSet$nextSet
					} ## if (pval >= alpha)
				} ## repeat
			} ## if (length_nbrs >= ord)
		} ## if(!done)
		
		# cleanup memory
		rm(nbrs)
		
		list(i, G_yx, sepset_yx, num_tests, pMax_yx, done_xy)
	}
	
	# determine number of processes/cores
	num_workers <- 16
	
	start_t<-proc.time()
	while (!done && any(G) && ord<m.max) {		
		n.edgetests[ord+1] <- 0
		done <- TRUE
		ind <- which(G, arr.ind = TRUE)
		## For comparison with C++ sort according to first row
		ind <- ind[order(ind[,1]) ,]
		##Thuc adds the following line to remove redundant tests. E.g 1,2 and 2, 1
		ind=subset(ind, ind[,1]<ind[,2])
		Gnbrs=G
		remainingEdgeTests <- nrow(ind)
		if(verbose)
			cat("Order=",ord,"; remaining edges:",remainingEdgeTests,"\n", sep='')
		
		# parallel CI tests
		if (num_workers > remainingEdgeTests) num_workers <- remainingEdgeTests
		res <- do.call(c, unname(mclapply(splitIndices(remainingEdgeTests, num_workers),
								function(l) lapply(l, testCI))))
#		res <- do.call(c, unname(mclapply(splitIndices(remainingEdgeTests, num_workers),
#								function(l) lapply(l, testCI),
#								mc.cores=num_workers, mc.preschedule=TRUE, mc.cleanup=TRUE,
#								mc.allow.recursive=FALSE, mc.set.seed=FALSE, mc.silent=TRUE)))
		
		#synchronize
		for (p_obj in res) {			
			i <- p_obj[[1]]
			n.edgetests[ord+1] <- n.edgetests[ord+1] + p_obj[[4]]
			NoCITest <- NoCITest + p_obj[[4]]
			pMax[ind[i,1], ind[i,2]] <- p_obj[[5]]
			G[ind[i,1], ind[i,2]] <- G[ind[i,2], ind[i,1]] <- p_obj[[2]]
			if (!p_obj[[2]]) {
				sepset[[ind[i,1]]][[ind[i,2]]] <- p_obj[[3]]
			}
			done <- done & p_obj[[6]]			
		}
		
		## # cleanup memory
		rm(res)
		## rm(i)
		rm(ind)
		rm(Gnbrs)
		## rm(remainingEdgeTests)
		## rm(p_obj)
		## gc()
		
		# display memory usage
		## print(paste('Round ', ord))
		## print(sort(sapply(ls(), function(object.name) object.size(get(object.name)))))
		
		# increase the nbrs size
		ord <- ord+1
		
		## if(verbose>2){
		##     timewhile<-proc.time()-startwhile
		##     print(timewhile)
		## }
		
	} ## while
	time_t = proc.time()-start_t
	print(time_t)
	
	time<-proc.time()-start
	print(time)
	
	#number of conditional independence tests
	cat("number of CI tests:", NoCITest, "n.edgetests", n.edgetests,"\n")
	
	for (i in 1:(p-1)) {
		for (j in 2:p) {
			pMax[i,j] <- pMax[j,i] <- max(pMax[i,j],pMax[j,i])
		} ## for (j in 2:p)
	} ## for (i in 1:(p-1))
	
	## transform matrix to graph object :
	if (sum(G) == 0) {
		Gobject <- new("graphNEL", nodes = as.character(1:p))
	} else {
		colnames(G) <- rownames(G) <- as.character(1:p)
		Gobject <- as(G,"graphNEL")
	}
	
	## final object
	new("pcAlgo",
			graph = Gobject,
			call = cl, n = integer(0), max.ord = as.integer(ord-1),
			n.edgetests = n.edgetests, sepset = sepset,
			pMax = pMax, zMin = matrix(NA,1,1))
} ## end{ skeleton }
