# This includes the implementations of Stable PC & Parallelized PC
# 
# Author: tao
###############################################################################

library(pcalg)
library(parallel)

pcStable <- function(suffStat, indepTest, p, alpha, verbose = FALSE, fixedGaps = NULL,
		fixedEdges = NULL, NAdelete = TRUE, m.max = Inf, u2pd = "rand") {
	
	## Initial Checks
	cl <- match.call()
	
	## Skeleton
	skel <- skeletonStable(suffStat, indepTest, p, alpha, verbose = verbose,
			fixedGaps = fixedGaps, fixedEdges = fixedEdges,
			NAdelete = NAdelete, m.max = m.max)
	
	## Orient edges
	res <- switch (u2pd, "rand" = udag2pdag(skel),
			"retry" = udag2pdagSpecial(skel)$pcObj,
			"relaxed" = udag2pdagRelaxed(skel))
	
	## Output
	res
}

skeletonStable <- function(suffStat, indepTest, p, alpha, verbose = FALSE,
		fixedGaps = NULL, fixedEdges = NULL, NAdelete = TRUE,
		m.max = Inf) {
	
	start<-proc.time()
	cl <- match.call()
	
	pval <- NULL
	NoCITest=0
	
	## start skeleton
	sepset <- vector("list",p)
	n.edgetests <- numeric(1)# final length = max { ord}
	
	## fixed gaps
	if (is.null(fixedGaps)) {
		## G := complete graph :
		G <- matrix(rep(TRUE,p*p), nrow = p, ncol = p)
		diag(G) <- FALSE
	} else {
		if (!(identical(dim(fixedGaps),c(p,p)))) {
			stop("Dimensions of the dataset and fixedGaps do not agree.")
		} else {
			if (fixedGaps != t(fixedGaps)) {
				stop("fixedGaps must be symmetric")
			}
			G <- !fixedGaps
		} ## if (!(identical(dim(fixedGaps),c(p,p))))
	} ## if(is.null(G))
	
	## fixed edges
	if (is.null(fixedEdges)) {
		fixedEdges <- matrix(rep(FALSE,p*p), nrow = p, ncol = p)
	} else {
		if (!(identical(dim(fixedEdges),c(p,p)))) {
			stop("Dimensions of the dataset and fixedEdges do not agree.")
		}
		if (fixedEdges != t(fixedEdges)) {
			stop("fixedEdges must be symmetric")
		}
	} ## if(is.null(fixedEdges))
	
	seq_p <- 1:p
	for (iList in 1:p) sepset[[iList]] <- vector("list",p)
	## save maximal p value
	pMax <- matrix(rep(-Inf,p*p),nrow=p,ncol=p)
	diag(pMax) <- 1
	
	done <- FALSE
	ord <- 0
	
	start_t<-proc.time()
	while (!done && any(G) && ord<m.max) {
		startwhile=proc.time()
		n.edgetests[ord+1] <- 0
		done <- TRUE
		ind <- which(G, arr.ind = TRUE)
		## For comparison with C++ sort according to first row
		ind <- ind[order(ind[,1]) ,]
		##Thuc adds the following line to remove redundant tests. E.g 1,2 and 2, 1
		ind=subset(ind, ind[,1]<ind[,2])
		Gnbrs=G
		remainingEdgeTests <- nrow(ind)
		if(verbose)
			cat("Order=",ord,"; remaining edges:",remainingEdgeTests,"\n", sep='')
		
		for (i in 1:remainingEdgeTests) {
			if(verbose) { if(i%%100==0) cat("|i=",i,"|iMax=",nrow(ind),"\n") }
			x <- ind[i,1]
			y <- ind[i,2]
			if (G[y,x] && (!fixedEdges[y,x])) {
				nbrsBool <- Gnbrs[,x]
				nbrsBool[y] <- FALSE
				nbrs <- seq_p[nbrsBool]
				#if(verbose) cat("neighbors of x", nbrs, "\n")
				length_nbrs <- length(nbrs)
				if (length_nbrs >= ord) {
					if (length_nbrs > ord) done <- FALSE
					S <- seq(length = ord)
					repeat { ## condition w.r.to all  nbrs[S] of size 'ord'
						n.edgetests[ord+1] <- n.edgetests[ord+1]+1
						if(verbose>2)   starttest<-proc.time()
						pval <- indepTest(x,y, nbrs[S], suffStat)
						
						
						NoCITest=NoCITest+1
						## pval <- dsepTest(x,y,nbrs[S],gTrue,jp = jp)
						if (verbose) cat("x=",x," y=",y," S=",nbrs[S],": pval =",pval,"\n")
						
						if(verbose>2){
							timetest<-proc.time()-starttest
							print(timetest)
						}
						
						if (is.na(pval)) pval <- ifelse(NAdelete,1,0)
						if (pval > pMax[x,y]) pMax[x,y] <- pval
						if(pval >= alpha) { # independent
							G[x,y] <- G[y,x] <- FALSE
							sepset[[x]][[y]] <- nbrs[S]
							break
						} else {
							nextSet <- getNextSet(length_nbrs, ord, S)
							if(nextSet$wasLast)
								break
							S <- nextSet$nextSet
						} ## if (pval >= alpha)
					} ## repeat
				} ## if (length_nbrs >= ord)
			} ## if(!done)
			
		} ## for(i in 1:remainingEdgeTests)	
		
		ord <- ord+1
		
		if(verbose>2){
			timewhile<-proc.time()-startwhile
			print(timewhile)
		}
		
	} ## while
	
	print(proc.time()-start_t)
	
	time<-proc.time()-start
	print(time)
	
	#number of conditional independence tests
	cat("number of CI tests:", NoCITest, "n.edgetests", n.edgetests,"\n")
	
	for (i in 1:(p-1)) {
		for (j in 2:p) {
			pMax[i,j] <- pMax[j,i] <- max(pMax[i,j],pMax[j,i])
		} ## for (j in 2:p)
	} ## for (i in 1:(p-1))
	
	## transform matrix to graph object :
	if (sum(G) == 0) {
		Gobject <- new("graphNEL", nodes = as.character(1:p))
	} else {
		colnames(G) <- rownames(G) <- as.character(1:p)
		Gobject <- as(G,"graphNEL")
	}
	
	## final object
	new("pcAlgo",
			graph = Gobject,
			call = cl, n = integer(0), max.ord = as.integer(ord-1),
			n.edgetests = n.edgetests, sepset = sepset,
			pMax = pMax, zMin = matrix(NA,1,1))
	
}## end{ skeleton }

pcParallel <- function(suffStat, indepTest, p, alpha, verbose = FALSE, fixedGaps = NULL,
		fixedEdges = NULL, NAdelete = TRUE, m.max = Inf, u2pd = "rand") {
	## Initial Checks
	cl <- match.call()
	
	## Skeleton
	skel <- skeletonParallel(suffStat, indepTest, p, alpha, verbose = verbose,
			fixedGaps = fixedGaps, fixedEdges = fixedEdges,
			NAdelete = NAdelete, m.max = m.max)
	
	## Orient edges
	res <- switch (u2pd, "rand" = udag2pdag(skel),
			"retry" = udag2pdagSpecial(skel)$pcObj,
			"relaxed" = udag2pdagRelaxed(skel))
	
	## Output
	res
}

# using multicore - mclapply()
skeletonParallel <- function(suffStat, indepTest, p, alpha, verbose = FALSE,
		fixedGaps = NULL, fixedEdges = NULL, NAdelete = TRUE,
		m.max = Inf) {
	start<-proc.time()
	cl <- match.call()
	
	pval <- NULL
	NoCITest=0
	
	## start skeleton
	sepset <- vector("list", p)
	n.edgetests <- numeric(1)# final length = max { ord}
	
	## fixed gaps
	if (is.null(fixedGaps)) {
		## G := complete graph :
		G <- matrix(rep(TRUE,p*p), nrow = p, ncol = p)
		diag(G) <- FALSE
	} else {
		if (!(identical(dim(fixedGaps),c(p,p)))) {
			stop("Dimensions of the dataset and fixedGaps do not agree.")
		} else {
			if (fixedGaps != t(fixedGaps)) {
				stop("fixedGaps must be symmetric")
			}
			G <- !fixedGaps
		} ## if (!(identical(dim(fixedGaps),c(p,p))))
	} ## if(is.null(G))
	
	## fixed edges
	if (is.null(fixedEdges)) {
		fixedEdges <- matrix(rep(FALSE,p*p), nrow = p, ncol = p)
	} else {
		if (!(identical(dim(fixedEdges),c(p,p)))) {
			stop("Dimensions of the dataset and fixedEdges do not agree.")
		}
		if (fixedEdges != t(fixedEdges)) {
			stop("fixedEdges must be symmetric")
		}
	} ## if(is.null(fixedEdges))
	
	#seq_p <- 1:p
	for (iList in 1:p) sepset[[iList]] <- vector("list",p)
	## save maximal p value
	pMax <- matrix(rep(-Inf,p*p),nrow=p,ncol=p)
	diag(pMax) <- 1
	
	done <- FALSE
	ord <- 0
	
	# force compiler to evaluate the parameters so that they are usable by testCI_pair
	## alpha
	## suffStat
	## p
	
	# define wrapper function for CI test of each pair
	testCI_pair <- function(i) {
		x <- ind[i,1]
		y <- ind[i,2]
		n.edgetests <- 0
		G_yx <- TRUE
		pMax_yx <- -Inf
		sepset_yx <- NULL
		seq_p <- 1:p
		if (G[y,x] && (!fixedEdges[y,x])) {
			nbrsBool <- Gnbrs[,x]
			nbrsBool[y] <- FALSE
			nbrs <- seq_p[nbrsBool]
			#if(verbose) cat("neighbors of x", nbrs, "\n")
			length_nbrs <- length(nbrs)
			if (length_nbrs >= ord) {
				if (length_nbrs > ord) done <- FALSE
				S <- seq(length = ord)
				repeat { ## condition w.r.to all  nbrs[S] of size 'ord'
					n.edgetests <- n.edgetests + 1
					pval <- indepTest(x,y, nbrs[S], suffStat)					
					if (is.na(pval)) pval <- ifelse(NAdelete,1,0)
					if (pval > pMax_yx) pMax_yx <- pval
					if(pval >= alpha) { # independent
						G_yx <- FALSE
						sepset_yx <- nbrs[S]
						break
					} else {
						nextSet <- getNextSet(length_nbrs, ord, S)
						if(nextSet$wasLast)
							break
						S <- nextSet$nextSet
					} ## if (pval >= alpha)
				} ## repeat
			} ## if (length_nbrs >= ord)
		} ## if(!done)
		
		list("x"=x, "y"=y, "G_yx"=G_yx, "sepset_yx"=sepset_yx,
		  "n.edgetests"=n.edgetests, "pMax_yx"=pMax_yx, "done"=done)
	}
	
	start_t<-proc.time()
	while (!done && any(G) && ord<m.max) {		
		n.edgetests[ord+1] <- 0
		done <- TRUE
		ind <- which(G, arr.ind = TRUE)
		## For comparison with C++ sort according to first row
		ind <- ind[order(ind[,1]) ,]
		##Thuc adds the following line to remove redundant tests. E.g 1,2 and 2, 1
		ind=subset(ind, ind[,1]<ind[,2])
		Gnbrs=G
		remainingEdgeTests <- nrow(ind)
		if(verbose)
			cat("Order=",ord,"; remaining edges:",remainingEdgeTests,"\n", sep='')
		
		res <- mclapply(1:remainingEdgeTests, testCI_pair, mc.cores=3)
		
		#synchronize
		for (p_obj in res) {
			n.edgetests[ord+1] <- n.edgetests[ord+1] + p_obj$n.edgetests
			NoCITest <- NoCITest + p_obj$n.edgetests
			pMax[p_obj$x, p_obj$y] <- p_obj$pMax_yx
			G[p_obj$x, p_obj$y] <- G[p_obj$y, p_obj$x] <- p_obj$G_yx
			if (!p_obj$G_yx) {
				sepset[[p_obj$x]][[p_obj$y]] <- p_obj$sepset_yx
			}
			done <- done & p_obj$done
		}
		
		# cleanup memory
		rm(res)
		gc()
		
		# increase the nbrs size
		ord <- ord+1
		
		## if(verbose>2){
		##     timewhile<-proc.time()-startwhile
		##     print(timewhile)
		## }
		
	} ## while
	time_t = proc.time()-start_t
	print(proc.time()-start_t)
	
	time<-proc.time()-start
	print(time)
	
	fileConn<-file("PC-Parallel-7-time.txt")
	write(time_t, fileConn)
	close(fileConn)
	
	#number of conditional independence tests
	cat("number of CI tests:", NoCITest, "n.edgetests", n.edgetests,"\n")
	
	for (i in 1:(p-1)) {
		for (j in 2:p) {
			pMax[i,j] <- pMax[j,i] <- max(pMax[i,j],pMax[j,i])
		} ## for (j in 2:p)
	} ## for (i in 1:(p-1))
	
	## transform matrix to graph object :
	if (sum(G) == 0) {
		Gobject <- new("graphNEL", nodes = as.character(1:p))
	} else {
		colnames(G) <- rownames(G) <- as.character(1:p)
		Gobject <- as(G,"graphNEL")
	}
	
	## final object
	new("pcAlgo",
			graph = Gobject,
			call = cl, n = integer(0), max.ord = as.integer(ord-1),
			n.edgetests = n.edgetests, sepset = sepset,
			pMax = pMax, zMin = matrix(NA,1,1))
} ## end{ skeleton }
