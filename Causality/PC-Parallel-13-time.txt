* num_cores = 1
    user   system  elapsed 
2610.416    0.292 2610.869 
    user   system  elapsed 
2611.758    0.307 2612.226 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28

* num_cores = 2
    user   system  elapsed 
2586.746    4.132 1347.084 
    user   system  elapsed 
2587.417    4.154 1347.777 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28 

* num_cores = 3
user   system  elapsed 
2621.249    5.673  932.666 
    user   system  elapsed 
2621.963    5.700  933.407 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28 

* num_cores = 4
     user   system  elapsed 
2633.386    6.657  729.578 
    user   system  elapsed 
2634.079    6.689  730.303 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28

* num_cores = 5
    user   system  elapsed 
2693.364    7.920  601.649 
    user   system  elapsed 
2694.037    7.945  602.348 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28

* num_cores = 6
    user   system  elapsed 
2676.711    7.929  516.852 
    user   system  elapsed 
2677.494    7.960  517.667 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28 

* num_cores = 7
    user   system  elapsed 
2713.370    8.716  458.332 
    user   system  elapsed 
2713.859    8.743  458.848 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28 

* num_cores = 8
    user   system  elapsed 
2756.482   10.584  429.013 
    user   system  elapsed 
2757.147   10.610  429.705 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28 

    user   system  elapsed 
2730.465   10.702  417.802 
    user   system  elapsed 
2730.957   10.731  418.323 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28 


* num_cores = 9
    user   system  elapsed 
2958.553   11.387  472.801 
    user   system  elapsed 
2959.264   11.423  473.548 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28

    user   system  elapsed 
3009.835   11.724  456.845 
    user   system  elapsed 
3010.496   11.743  457.525 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28

* num cores = 10
    user   system  elapsed 
3129.882   12.807  433.953 
    user   system  elapsed 
3130.539   12.831  434.634 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28 

* num_cores = 11
    user   system  elapsed 
3312.698   14.668  413.027 
    user   system  elapsed 
3313.182   14.697  413.539 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28 

* num_cores = 12
    user   system  elapsed 
3536.566   16.858  400.231 
    user   system  elapsed 
3537.270   16.897  400.974 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28 


* num_cores = 13
    user   system  elapsed 
3722.115   17.777  390.416 
    user   system  elapsed 
3722.769   17.808  391.102 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28 

* num_cores = 14
    user   system  elapsed 
3892.524   20.813  367.148 
    user   system  elapsed 
3893.269   20.841  367.920 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28 

* num_cores = 15
    user   system  elapsed 
3719.755   54.900  433.222 
    user   system  elapsed 
3720.240   54.929  433.753 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28

* num_cores = 16
    user   system  elapsed 
4435.984   35.735  371.630 
    user   system  elapsed 
4436.724   35.768  372.432 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28 

    user   system  elapsed  (snpEnrichment - mclapply2)
2953.554   20.365  438.629 
    user   system  elapsed 
2954.941   20.389  440.042 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28 

    user   system  elapsed  (divide data into small chunks)
5323.698 2133.247  878.221 
    user   system  elapsed 
5324.259 2133.273  878.808 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28 

