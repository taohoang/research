library(pcalg)
#library(bnlearn)
#library(arules)
#library(hash)
######### This is the modified version of the PC algorithm as suggested in (Colombo, 2012) (refer to the main text for details). 
######### The implementation is based on the source code of the pcalg package. 
pc <- function(suffStat, indepTest, p, alpha, verbose = FALSE, fixedGaps = NULL,
               fixedEdges = NULL, NAdelete = TRUE, m.max = Inf,
               u2pd = "rand") {
  
  
  ## Initial Checks
  cl <- match.call()
  
  ## Skeleton
  skel <- pcalg::skeleton(suffStat, indepTest, p, alpha, verbose = verbose,
                          fixedGaps = fixedGaps, fixedEdges = fixedEdges,
                          NAdelete = NAdelete, m.max = m.max)
  
  ## Orient edges
  res <- switch (u2pd,
                 "rand" = udag2pdag(skel),
                 "retry" = udag2pdagSpecial(skel)$pcObj,
                 "relaxed" = udag2pdagRelaxed(skel))
  
  ## Output
  res
}
##############
pcStable <- function(suffStat, indepTest, p, alpha, verbose = FALSE, fixedGaps = NULL,
                     fixedEdges = NULL, NAdelete = TRUE, m.max = Inf,
                     u2pd = "rand") {
  
  
  ## Initial Checks
  cl <- match.call()
  
  ## Skeleton
  skel <- skeletonStable(suffStat, indepTest, p, alpha, verbose = verbose,
                         fixedGaps = fixedGaps, fixedEdges = fixedEdges,
                         NAdelete = NAdelete, m.max = m.max)
  
  ## Orient edges
  res <- switch (u2pd,
                 "rand" = udag2pdag(skel),
                 "retry" = udag2pdagSpecial(skel)$pcObj,
                 "relaxed" = udag2pdagRelaxed(skel))
  
  ## Output
  res
}
##############

#################################
pcPermut <- function(dataset, indepTest, p, alpha, B, verbose = FALSE, fixedGaps = NULL,
                     fixedEdges = NULL, NAdelete = TRUE, m.max = Inf,
                     u2pd = "rand") {
  
  
  
  ## Initial Checks
  cl <- match.call()
  
  ## Skeleton
  skel <- skeletonPermut(dataset, indepTest, p, alpha, B, verbose = verbose,
                         fixedGaps = fixedGaps, fixedEdges = fixedEdges,
                         NAdelete = NAdelete, m.max = m.max)
  
  ## Orient edges
  res <- switch (u2pd,
                 "rand" = udag2pdag(skel),
                 "retry" = udag2pdagSpecial(skel)$pcObj,
                 "relaxed" = udag2pdagRelaxed(skel))
  
  ## Output
  res
}
###################################################################################

pcPermutStable <- function(dataset, indepTest, p, alpha, B, verbose = FALSE, fixedGaps = NULL,
                           fixedEdges = NULL, NAdelete = TRUE, m.max = Inf,
                           u2pd = "rand") {
  
  
  
  ## Initial Checks
  cl <- match.call()
  
  ## Skeleton
  skel <- skeletonStablePermut(dataset, indepTest, p, alpha, B, verbose = verbose,
                               fixedGaps = fixedGaps, fixedEdges = fixedEdges,
                               NAdelete = NAdelete, m.max = m.max)
  
  ## Orient edges
  res <- switch (u2pd,
                 "rand" = udag2pdag(skel),
                 "retry" = udag2pdagSpecial(skel)$pcObj,
                 "relaxed" = udag2pdagRelaxed(skel))
  
  ## Output
  res
}



####################################################################################
skeletonStable <- function(suffStat, indepTest, p, alpha, verbose = FALSE,
                           fixedGaps = NULL, fixedEdges = NULL, NAdelete = TRUE,
                           m.max = Inf) {
  
  
  
  start<-proc.time()
  cl <- match.call()
  
  pval <- NULL
  NoCITest=0
  ## start skeleton
  sepset <- vector("list",p)
  n.edgetests <- numeric(1)# final length = max { ord}
  
  ## fixed gaps
  if (is.null(fixedGaps)) {
    ## G := complete graph :
    G <- matrix(rep(TRUE,p*p), nrow = p, ncol = p)
    diag(G) <- FALSE
  } else {
    if (!(identical(dim(fixedGaps),c(p,p)))) {
      stop("Dimensions of the dataset and fixedGaps do not agree.")
    } else {
      if (fixedGaps != t(fixedGaps)) {
        stop("fixedGaps must be symmetric")
      }
      G <- !fixedGaps
    } ## if (!(identical(dim(fixedGaps),c(p,p))))
  } ## if(is.null(G))
  
  ## fixed edges
  if (is.null(fixedEdges)) {
    fixedEdges <- matrix(rep(FALSE,p*p), nrow = p, ncol = p)
  } else {
    if (!(identical(dim(fixedEdges),c(p,p)))) {
      stop("Dimensions of the dataset and fixedEdges do not agree.")
    }
    if (fixedEdges != t(fixedEdges)) {
      stop("fixedEdges must be symmetric")
    }
  } ## if(is.null(fixedEdges))
  
  seq_p <- 1:p
  for (iList in 1:p) sepset[[iList]] <- vector("list",p)
  ## save maximal p value
  pMax <- matrix(rep(-Inf,p*p),nrow=p,ncol=p)
  diag(pMax) <- 1
  
  done <- FALSE
  ord <- 0
  
  while (!done && any(G) && ord<m.max) {
    startwhile=proc.time()
    n.edgetests[ord+1] <- 0
    done <- TRUE
    ind <- which(G, arr.ind = TRUE)
    ## For comparison with C++ sort according to first row
    ind <- ind[order(ind[,1]) ,]
    ##Thuc adds the following line to remove redundant tests. E.g 1,2 and 2, 1
    ind=subset(ind, ind[,1]<ind[,2])
    Gnbrs=G
    remainingEdgeTests <- nrow(ind)
    if(verbose)
      cat("Order=",ord,"; remaining edges:",remainingEdgeTests,"\n", sep='')
    for (i in 1:remainingEdgeTests) {
      if(verbose) { if(i%%100==0) cat("|i=",i,"|iMax=",nrow(ind),"\n") }
      x <- ind[i,1]
      y <- ind[i,2]
      if (G[y,x] && (!fixedEdges[y,x])) {
        nbrsBool <- Gnbrs[,x]
        nbrsBool[y] <- FALSE
        nbrs <- seq_p[nbrsBool]
        #if(verbose) cat("neighbors of x", nbrs, "\n")
        length_nbrs <- length(nbrs)
        if (length_nbrs >= ord) {
          if (length_nbrs > ord) done <- FALSE
          S <- seq(length = ord)
          repeat { ## condition w.r.to all  nbrs[S] of size 'ord'
            n.edgetests[ord+1] <- n.edgetests[ord+1]+1
            if(verbose>2)   starttest<-proc.time()
            pval <- indepTest(x,y, nbrs[S], suffStat)
            
            NoCITest=NoCITest+1
            ## pval <- dsepTest(x,y,nbrs[S],gTrue,jp = jp)
            if (verbose) cat("x=",x," y=",y," S=",nbrs[S],": pval =",pval,"\n")
            
            if(verbose>2){
              timetest<-proc.time()-starttest
              print(timetest)
            }
            
            if (is.na(pval)) pval <- ifelse(NAdelete,1,0)
            if (pval > pMax[x,y]) pMax[x,y] <- pval
            if(pval >= alpha) { # independent
              G[x,y] <- G[y,x] <- FALSE
              sepset[[x]][[y]] <- nbrs[S]
              break
            } else {
              nextSet <- getNextSet(length_nbrs, ord, S)
              if(nextSet$wasLast)
                break
              S <- nextSet$nextSet
            } ## if (pval >= alpha)
          } ## repeat
        } ## if (length_nbrs >= ord)
      } ## if(!done)
      
    } ## for(i in 1:remainingEdgeTests)
    ord <- ord+1
    
    if(verbose>2){
      timewhile<-proc.time()-startwhile
      print(timewhile)
    }
    
  } ## while
  time<-proc.time()-start
  print(time)
  #number of conditional independence tests
  cat("number of CI tests:", NoCITest, "n.edgetests", n.edgetests,"\n")
  
  for (i in 1:(p-1)) {
    for (j in 2:p) {
      pMax[i,j] <- pMax[j,i] <- max(pMax[i,j],pMax[j,i])
    } ## for (j in 2:p)
  } ## for (i in 1:(p-1))
  
  ## transform matrix to graph object :
  if (sum(G) == 0) {
    Gobject <- new("graphNEL", nodes = as.character(1:p))
  } else {
    colnames(G) <- rownames(G) <- as.character(1:p)
    Gobject <- as(G,"graphNEL")
  }
  
  ## final object
  new("pcAlgo",
      graph = Gobject,
      call = cl, n = integer(0), max.ord = as.integer(ord-1),
      n.edgetests = n.edgetests, sepset = sepset,
      pMax = pMax, zMin = matrix(NA,1,1))
  
}## end{ skeleton }

###################################################################################


skeletonPermut <- function(dataset,  indepTest, p, alpha, B, verbose = FALSE,
                           fixedGaps = NULL, fixedEdges = NULL, NAdelete = TRUE,
                           m.max = Inf) {
  
  
  
  cl <- match.call()
  
  pval <- NULL
  
  suffStat=list(C=cor(dataset), n=nrow(dataset))	
  ## start skeleton
  sepset <- vector("list",p)
  n.edgetests <- numeric(1)# final length = max { ord}
  
  ## fixed gaps
  if (is.null(fixedGaps)) {
    ## G := complete graph :
    G <- matrix(rep(TRUE,p*p), nrow = p, ncol = p)
    diag(G) <- FALSE
  } else {
    if (!(identical(dim(fixedGaps),c(p,p)))) {
      stop("Dimensions of the dataset and fixedGaps do not agree.")
    } else {
      if (fixedGaps != t(fixedGaps)) {
        stop("fixedGaps must be symmetric")
      }
      G <- !fixedGaps
    } ## if (!(identical(dim(fixedGaps),c(p,p))))
  } ## if(is.null(G))
  
  ## fixed edges
  if (is.null(fixedEdges)) {
    fixedEdges <- matrix(rep(FALSE,p*p), nrow = p, ncol = p)
  } else {
    if (!(identical(dim(fixedEdges),c(p,p)))) {
      stop("Dimensions of the dataset and fixedEdges do not agree.")
    }
    if (fixedEdges != t(fixedEdges)) {
      stop("fixedEdges must be symmetric")
    }
  } ## if(is.null(fixedEdges))
  
  seq_p <- 1:p
  for (iList in 1:p) sepset[[iList]] <- vector("list",p)
  ## save maximal p value
  pMax <- matrix(rep(-Inf,p*p),nrow=p,ncol=p)
  diag(pMax) <- 1
  
  done <- FALSE
  ord <- 0
  
  while (!done && any(G) && ord<=m.max) {
    n.edgetests[ord+1] <- 0
    done <- TRUE
    ind <- which(G, arr.ind = TRUE)
    ## For comparison with C++ sort according to first row
    ind <- ind[order(ind[,1]) ,]
    remainingEdgeTests <- nrow(ind)
    if(verbose)
      cat("Order=",ord,"; remaining edges:",remainingEdgeTests,"\n", sep='')
    for (i in 1:remainingEdgeTests) {
      if(verbose) { if(i%%100==0) cat("|i=",i,"|iMax=",nrow(ind),"\n") }
      x <- ind[i,1]
      y <- ind[i,2]
      if (G[y,x] && (!fixedEdges[y,x])) {
        nbrsBool <- G[,x]
        nbrsBool[y] <- FALSE
        nbrs <- seq_p[nbrsBool]
        length_nbrs <- length(nbrs)
        if (length_nbrs >= ord) {
          if (length_nbrs > ord) done <- FALSE
          S <- seq(length = ord)
          repeat { ## condition w.r.to all  nbrs[S] of size 'ord'
            n.edgetests[ord+1] <- n.edgetests[ord+1]+1
            
            pval <- gaussCItest(x,y, nbrs[S], suffStat)
            if(2==1){
              if(!is.data.frame(dataset)) dataset=data.frame(dataset)
              Namex=colnames(dataset)[x]
              Namey=colnames(dataset)[y]
              Namez=colnames(dataset)[nbrs[S]]
              #test=bnlearn::ci.test(x=Namex, y=Namey, z=Namez, data=dataset, test=indepTestA)
              #pval=test$p.value
              
              cat("x=",x," y=",y," S=",nbrs[S], "pvalue=", pval, " Now we try the permutation  ---- ")
              test=bnlearn::ci.test(x=Namex, y=Namey, z=Namez, data=dataset, test=indepTest, B=B)
              pval=test$p.value
              cat("new pvalue=", pval, "\n")
            }
            
            
            #pval <- indepTest(x,y, nbrs[S], suffStat)
            
            ## pval <- dsepTest(x,y,nbrs[S],gTrue,jp = jp)
            cat("x=",x," y=",y," S=",nbrs[S],": pval =",pval,"\n")
            if (is.na(pval)) pval <- ifelse(NAdelete,1,0)
            if (pval > pMax[x,y]) pMax[x,y] <- pval
            if(pval >= alpha) { # independent
              G[x,y] <- G[y,x] <- FALSE
              sepset[[x]][[y]] <- nbrs[S]
              break
            } else {
              nextSet <- getNextSet(length_nbrs, ord, S)
              if(nextSet$wasLast)
                break
              S <- nextSet$nextSet
            } ## if (pval >= alpha)
          } ## repeat
        } ## if (length_nbrs >= ord)
      } ## if(!done)
      
    } ## for(i in 1:remainingEdgeTests)
    ord <- ord+1
  } ## while
  
  for (i in 1:(p-1)) {
    for (j in 2:p) {
      pMax[i,j] <- pMax[j,i] <- max(pMax[i,j],pMax[j,i])
    } ## for (j in 2:p)
  } ## for (i in 1:(p-1))
  
  ## transform matrix to graph object :
  if (sum(G) == 0) {
    Gobject <- new("graphNEL", nodes = as.character(1:p))
  } else {
    colnames(G) <- rownames(G) <- as.character(1:p)
    Gobject <- as(G,"graphNEL")
  }
  
  ## final object
  new("pcAlgo",
      graph = Gobject,
      call = cl, n = integer(0), max.ord = as.integer(ord-1),
      n.edgetests = n.edgetests, sepset = sepset,
      pMax = pMax, zMin = matrix(NA,1,1))
  
}## end{ skeleton }

skeletonStablePermut <- function(dataset, indepTest, p, alpha,B, verbose = FALSE,
                                 fixedGaps = NULL, fixedEdges = NULL, NAdelete = TRUE,
                                 m.max = Inf) {
  
  
  
  start<-proc.time()
  cl <- match.call()
  
  suffStat=list(C=cor(dataset), n=nrow(dataset))
  pval <- NULL
  NoCITest=0
  ## start skeleton
  sepset <- vector("list",p)
  n.edgetests <- numeric(1)# final length = max { ord}
  
  ## fixed gaps
  if (is.null(fixedGaps)) {
    ## G := complete graph :
    G <- matrix(rep(TRUE,p*p), nrow = p, ncol = p)
    diag(G) <- FALSE
  } else {
    if (!(identical(dim(fixedGaps),c(p,p)))) {
      stop("Dimensions of the dataset and fixedGaps do not agree.")
    } else {
      if (fixedGaps != t(fixedGaps)) {
        stop("fixedGaps must be symmetric")
      }
      G <- !fixedGaps
    } ## if (!(identical(dim(fixedGaps),c(p,p))))
  } ## if(is.null(G))
  
  ## fixed edges
  if (is.null(fixedEdges)) {
    fixedEdges <- matrix(rep(FALSE,p*p), nrow = p, ncol = p)
  } else {
    if (!(identical(dim(fixedEdges),c(p,p)))) {
      stop("Dimensions of the dataset and fixedEdges do not agree.")
    }
    if (fixedEdges != t(fixedEdges)) {
      stop("fixedEdges must be symmetric")
    }
  } ## if(is.null(fixedEdges))
  
  seq_p <- 1:p
  for (iList in 1:p) sepset[[iList]] <- vector("list",p)
  ## save maximal p value
  pMax <- matrix(rep(-Inf,p*p),nrow=p,ncol=p)
  diag(pMax) <- 1
  
  done <- FALSE
  ord <- 0
  
  while (!done && any(G) && ord<m.max) {
    startwhile=proc.time()
    n.edgetests[ord+1] <- 0
    done <- TRUE
    ind <- which(G, arr.ind = TRUE)
    ## For comparison with C++ sort according to first row
    ind <- ind[order(ind[,1]) ,]
    ##Thuc adds the following line to remove redundant tests. E.g 1,2 and 2, 1
    ind=subset(ind, ind[,1]<ind[,2])
    Gnbrs=G
    remainingEdgeTests <- nrow(ind)
    if(verbose)
      cat("Order=",ord,"; remaining edges:",remainingEdgeTests,"\n", sep='')
    for (i in 1:remainingEdgeTests) {
      if(verbose) { if(i%%100==0) cat("|i=",i,"|iMax=",nrow(ind),"\n") }
      x <- ind[i,1]
      y <- ind[i,2]
      if (G[y,x] && (!fixedEdges[y,x])) {
        nbrsBool <- Gnbrs[,x]
        nbrsBool[y] <- FALSE
        nbrs <- seq_p[nbrsBool]
        #if(verbose) cat("neighbors of x", nbrs, "\n")
        length_nbrs <- length(nbrs)
        if (length_nbrs >= ord) {
          if (length_nbrs > ord) done <- FALSE
          S <- seq(length = ord)
          repeat { ## condition w.r.to all  nbrs[S] of size 'ord'
            n.edgetests[ord+1] <- n.edgetests[ord+1]+1
            if(verbose>2)   starttest<-proc.time()
            
            pval <- gaussCItest(x,y, nbrs[S], suffStat)
            if(2==1){
              if(!is.data.frame(dataset)) dataset=data.frame(dataset)
              Namex=colnames(dataset)[x]
              Namey=colnames(dataset)[y]
              Namez=colnames(dataset)[nbrs[S]]
              #test=bnlearn::ci.test(x=Namex, y=Namey, z=Namez, data=dataset, test=indepTestA)
              #pval=test$p.value
              
              cat("x=",x," y=",y," S=",nbrs[S], "pvalue=", pval, " Now we try the permutation ")
              test=bnlearn::ci.test(x=Namex, y=Namey, z=Namez, data=dataset, test=indepTest, B=B)
              pval=test$p.value
              cat("new pvalue=", pval, "\n")
            }
            
            #pval <- indepTest(x,y, nbrs[S], suffStat)
            
            
            NoCITest=NoCITest+1
            ## pval <- dsepTest(x,y,nbrs[S],gTrue,jp = jp)
            if(verbose)  cat("x=",x," y=",y," S=",nbrs[S],": pval =",pval,"\n")
            
            if(verbose>2){
              timetest<-proc.time()-starttest
              print(timetest)
            }
            
            if (is.na(pval)) pval <- ifelse(NAdelete,1,0)
            if (pval > pMax[x,y]) pMax[x,y] <- pval
            if(pval >= alpha) { # independent
              G[x,y] <- G[y,x] <- FALSE
              sepset[[x]][[y]] <- nbrs[S]
              break
            } else {
              nextSet <- getNextSet(length_nbrs, ord, S)
              if(nextSet$wasLast)
                break
              S <- nextSet$nextSet
            } ## if (pval >= alpha)
          } ## repeat
        } ## if (length_nbrs >= ord)
      } ## if(!done)
      
    } ## for(i in 1:remainingEdgeTests)
    ord <- ord+1
    
    if(verbose>2){
      timewhile<-proc.time()-startwhile
      print(timewhile)
    }
    
  } ## while
  time<-proc.time()-start
  print(time)
  #number of conditional independence tests
  cat("number of CI tests:", NoCITest, "n.edgetests", n.edgetests,"\n")
  
  for (i in 1:(p-1)) {
    for (j in 2:p) {
      pMax[i,j] <- pMax[j,i] <- max(pMax[i,j],pMax[j,i])
    } ## for (j in 2:p)
  } ## for (i in 1:(p-1))
  
  ## transform matrix to graph object :
  if (sum(G) == 0) {
    Gobject <- new("graphNEL", nodes = as.character(1:p))
  } else {
    colnames(G) <- rownames(G) <- as.character(1:p)
    Gobject <- as(G,"graphNEL")
  }
  
  ## final object
  new("pcAlgo",
      graph = Gobject,
      call = cl, n = integer(0), max.ord = as.integer(ord-1),
      n.edgetests = n.edgetests, sepset = sepset,
      pMax = pMax, zMin = matrix(NA,1,1))
  
}## end{ skeleton }


