# Test different parallelization libraries on computing (sqrt(x) + 10000) / 13 for x = 1 to 10000
# 
# Author: tao
###############################################################################


# Method 1: sequential
start <- proc.time()
y <- list()
for (x in 1:10000) {
	y[x] <- (sqrt(x) + 10000) / 13
}
print(proc.time()-start) # 0.411, 0.396, 0.395, 0.409, 0.389

# Method 2: parallelization using parallel (no grouping)
library(parallel)
num_workers = 4;
workerFunc <- function(x) {
	y_local <- (sqrt(x) + 10000) / 13
	list(x, y_local)
}
cl <- makeCluster(num_workers)
start <- proc.time()
y <- list()
res <- parLapply(cl, 1:10000, workerFunc)
for (r in res) {
	x <- r[[1]]
	y_local <- r[[2]]
	y[x] <- y_local
}
print(proc.time()-start) # 435, 441, 445, 

# Method 3: parallelization using parallel (with grouping)
library(parallel)
num_workers = 4;
workerFunc <- function(x) {
	y_local <- list()
	for (i in x$s:x$e) {
		y_local[i] <- (sqrt(i) + 10000) / 13
	}
	list(x, y_local)
}
cl <- makeCluster(num_workers)
start <- proc.time()
y <- list()
t <- list(list(s=1, e=2500), list(s=2501, e=5000), list(s=5001, e=7500), list(s=7501, e=10000))
res <- parLapply(cl, t, workerFunc)
for (r in res) {
	x <- r[[1]]
	y_local <- r[[2]]
	for (i in x$s:x$e) {
		y[i] <- y_local[i]
	}
}
print(proc.time()-start) # 0.355, 0.178, 0.162, 0.176, 0.171 -> 0.208
stopCluster(cl)

# Method 4: parallelization using doParallel & foreach
library(doParallel)
library(foreach)
library(plyr)
num_workers = 4;
cl <- makeCluster(num_workers)
registerDoParallel(cl)
start <- proc.time()
y <- list()
res <- foreach(i=1:5000) %dopar% sqrt(i)  #run a loop in parallel
y <- res
print(proc.time()-start) # 3.211


# Example of bootstrapping
library(doParallel)
cl <- makeCluster(4)
registerDoParallel(cl)

x <- iris[which(iris[,5] != "setosa"), c(1,5)]
trials <- 10000
ptime <- system.time({
	r <- foreach(icount(trials), .combine=cbind) %dopar% {
		ind <- sample(100, 100, replace=TRUE)
		result1 <- glm(x[ind,2]~x[ind,1], family=binomial(logit))
		coefficients(result1)
	}
})[3]
print(ptime)

stime <- system.time({
	r <- foreach(icount(trials), .combine=cbind) %do% {
		ind <- sample(100, 100, replace=TRUE)
		result1 <- glm(x[ind,2]~x[ind,1], family=binomial(logit))
		coefficients(result1)
	}
})[3]
print(stime)

stopCluster(cl)

# test
library(parallel)
cl <- makeCluster(4)



stopCluster(cl)

