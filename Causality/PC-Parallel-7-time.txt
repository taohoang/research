* num_cores = 1
    user   system  elapsed 
2610.416    0.292 2610.869 
    user   system  elapsed 
2611.758    0.307 2612.226 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28 

* num_cores = 2
user   system  elapsed 
2820.121    7.882 1581.160 
    user   system  elapsed 
2821.615    7.904 1582.673 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28 

* num_cores = 3 
    user   system  elapsed 
2600.997   10.128 1091.044 
    user   system  elapsed 
2602.393   10.143 1092.450 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28

* num_cores = 4
    user   system  elapsed 
2594.066   12.372  886.729 
    user   system  elapsed 
2595.464   12.389  888.143 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28

* num_cores = 5
    user   system  elapsed 
2673.584   15.857  823.957 
    user   system  elapsed 
2674.933   15.868  825.316 
number of CI tests: 21433907 n.edgetests 1014600 19890119 527662 867 463 168 28
