library(pcalg)
library(parallel)

#### Parallelized PC algorithm ####
pc_parallel <- function(suffStat, indepTest, alpha, labels, p,
		fixedGaps = NULL, fixedEdges = NULL, NAdelete = TRUE, m.max = Inf,
		u2pd = c("relaxed", "rand", "retry"),
		skel.method = c("parallel"),
		conservative = FALSE, maj.rule = FALSE,
		solve.confl = FALSE, verbose = FALSE)
{	
	## Initial Checks
	cl <- match.call()
	if(!missing(p)) stopifnot(is.numeric(p), length(p <- as.integer(p)) == 1, p >= 2)
	if(missing(labels)) {
		if(missing(p)) stop("need to specify 'labels' or 'p'")
		labels <- as.character(seq_len(p))
	} else { ## use labels ==> p  from it
		stopifnot(is.character(labels))
		if(missing(p)) {
			p <- length(labels)
		} else if(p != length(labels))
			stop("'p' is not needed when 'labels' is specified, and must match length(labels)")
		else
			message("No need to specify 'p', when 'labels' is given")
	}
	seq_p <- seq_len(p)
	
	u2pd <- match.arg(u2pd)
	skel.method <- match.arg(skel.method)
	if(u2pd != "relaxed") {
		if (conservative || maj.rule)
			stop("Conservative PC and majority rule PC can only be run with 'u2pd = relaxed'")
		
		if (solve.confl)
			stop("Versions of PC using lists for the orientation rules (and possibly bi-directed edges)\n can only be run with 'u2pd = relaxed'")
	}
	
	if (conservative && maj.rule) stop("Choose either conservative PC or majority rule PC!")
	
	## Skeleton
	skel <- skeleton_parallel(suffStat, indepTest, alpha, labels=labels, method = skel.method,
			fixedGaps=fixedGaps, fixedEdges=fixedEdges,
			NAdelete=NAdelete, m.max=m.max, verbose=verbose)
	skel@call <- cl # so that makes it into result
	
	## Orient edges
	if (!conservative && !maj.rule) {
		switch (u2pd,
				"rand" = udag2pdag(skel),
				"retry" = udag2pdagSpecial(skel)$pcObj,
				"relaxed" = udag2pdagRelaxed(skel, verbose=verbose))
	}
	else { ## u2pd "relaxed" : conservative _or_ maj.rule
		
		## version.unf defined per default
		## Tetrad CPC works with version.unf=c(2,1)
		## see comment on pc.cons.intern for description of version.unf
		pc. <- pc.cons.intern(skel, suffStat, indepTest, alpha,
				version.unf=c(2,1), maj.rule=maj.rule, verbose=verbose)
		udag2pdagRelaxed(pc.$sk, verbose=verbose,
				unfVect=pc.$unfTripl)
	}
}


#### Parallelized skeleton estimation ####
skeleton_parallel <- function(suffStat, indepTest, alpha, labels, p,
		method = c("parallel"),
		m.max = Inf, fixedGaps = NULL, fixedEdges = NULL,
		NAdelete = TRUE, verbose = FALSE)
{	
	cl <- match.call()
	if(!missing(p)) stopifnot(is.numeric(p), length(p <- as.integer(p)) == 1, p >= 2)
	if(missing(labels)) {
		if(missing(p)) stop("need to specify 'labels' or 'p'")
		labels <- as.character(seq_len(p))
	} else { ## use labels ==> p  from it
		stopifnot(is.character(labels))
		if(missing(p)) {
			p <- length(labels)
		} else if(p != length(labels))
			stop("'p' is not needed when 'labels' is specified, and must match length(labels)")
		else
			message("No need to specify 'p', when 'labels' is given")
	}
	seq_p <- seq_len(p)
	method <- match.arg(method)
	
	## G := !fixedGaps, i.e. G[i,j] is true  iff  i--j  will be investigated
	if (is.null(fixedGaps)) {
		G <- matrix(TRUE, nrow = p, ncol = p)
		diag(G) <- FALSE
	}
	else if (!identical(dim(fixedGaps), c(p, p)))
		stop("Dimensions of the dataset and fixedGaps do not agree.")
	else if (!identical(fixedGaps, t(fixedGaps)) )
		stop("fixedGaps must be symmetric")
	else
		G <- !fixedGaps
	
	if (any(is.null(fixedEdges))) { ## MM: could be sparse
		fixedEdges <- matrix(rep(FALSE, p * p), nrow = p, ncol = p)
	}
	else if (!identical(dim(fixedEdges), c(p, p)))
		stop("Dimensions of the dataset and fixedEdges do not agree.")
	else if (fixedEdges != t(fixedEdges))
		stop("fixedEdges must be symmetric")
	
	pval <- NULL
	sepset <- lapply(seq_p, function(.) vector("list",p))# a list of lists [p x p]
	## save maximal p value
	pMax <- matrix(-Inf, nrow = p, ncol = p)
	diag(pMax) <- 1
	done <- FALSE
	ord <- 0
	n.edgetests <- numeric(1)# final length = max { ord}
  
	# define the edge test function
  edge_test_xy <- function(x, y) {
    G_xy <- TRUE
    num_tests_xy <- 0
    pMax_xy <- pMax[x, y]
    sepset_xy <- NULL
    done_xy <- TRUE
    if (G_xy && !fixedEdges[y, x]) {
      nbrsBool <- G.l[[x]]
      nbrsBool[y] <- FALSE
      nbrs <- seq_p[nbrsBool]
      length_nbrs <- length(nbrs)
      if (length_nbrs >= ord) {
        if (length_nbrs > ord) done_xy <- FALSE
        S <- seq_len(ord)
        repeat { ## condition w.r.to all  nbrs[S] of size 'ord'
          num_tests_xy <- num_tests_xy + 1  				
          pval <- indepTest(x, y, nbrs[S], suffStat)
          if(is.na(pval)) pval <- as.numeric(NAdelete) ## = if(NAdelete) 1 else 0
          if (pMax_xy < pval) pMax_xy <- pval
          if(pval >= alpha) { # independent
            G_xy <- FALSE
            sepset_xy <- nbrs[S]
            break
          } else {
            nextSet <- getNextSet(length_nbrs, ord, S)
            if (nextSet$wasLast)
              break
            S <- nextSet$nextSet
          } ## if (pval >= alpha)
        } ## repeat
      } ## if (length_nbrs >= ord)
    } ## if(!done)
    list(G_xy, sepset_xy, num_tests_xy, pMax_xy, done_xy)
  }
	edge_test <- function(i) {
		x <- ind[i, 1]
		y <- ind[i, 2]
		num_tests_i <- 0
		G_i <- TRUE
    pMax_xy <- pMax[x, y]
    pMax_yx <- pMax[y, x]
		sepset_xy <- NULL
    sepset_yx <- NULL
		done_i <- TRUE
    
    # consider neighbors of x
    res_x <- edge_test_xy(x, y)
    G_i <- res_x[[1]]
    sepset_xy <- res_x[[2]]
    num_tests_i <- num_tests_i + res_x[[3]]
    pMax_xy <- res_x[[4]]
    done_i <- done_i & res_x[[5]]
    
    if (G_i) {
      # consider neighbors of y
      res_y <- edge_test_xy(y, x)
      G_i <- res_y[[1]]
      sepset_yx <- res_y[[2]]
      num_tests_i <- num_tests_i + res_y[[3]]
      pMax_yx <- res_y[[4]]
      done_i <- done_i & res_y[[5]]
    }
				
		list(i, G_i, sepset_xy, sepset_yx, num_tests_i, pMax_xy, pMax_yx, done_i)
	}
  
	start_total <- proc.time()
  
	# prepare workers
	num_workers <- 8
  worker_type <- if (exists("mcfork", mode="function")) "FORK" else "PSOCK"
	workers <- makeCluster(num_workers, type=worker_type)
	eval(suffStat)
	clusterEvalQ(workers, library(pcalg))
	
	while (!done && any(G) && ord <= m.max) {
		n.edgetests[ord1 <- ord+1L] <- 0
		done <- TRUE
		ind <- which(G, arr.ind = TRUE)
		## For comparison with C++ sort according to first row
		ind <- ind[order(ind[, 1]), ]
		## Consider only unique edge
		ind <- subset(ind, ind[, 1] < ind[, 2])
		remainingEdgeTests <- nrow(ind)
		## Order-independent version: Compute the adjacency sets for any vertex
		## Then don't update when edges are deleted
		G.l <- split(G, gl(p,p))
		
		# parallel CI tests		
		res <- parLapply(workers, 1:remainingEdgeTests, edge_test)
		
		#synchronize
		for (p_obj in res) {
			i <- p_obj[[1]]
      x <- ind[i, 1]
      y <- ind[i, 2]
			n.edgetests[ord1] <- n.edgetests[ord1] + p_obj[[5]]
			pMax[x, y] <- p_obj[[6]]
			pMax[y, x] <- p_obj[[7]]
			G[x, y] <- G[y, x] <- p_obj[[2]]
			if (!p_obj[[2]]) {
        if (!is.null(p_obj[[3]])) sepset[[x]][[y]] <- p_obj[[3]]
        if (!is.null(p_obj[[4]])) sepset[[y]][[x]] <- p_obj[[4]]
			}
			done <- done & p_obj[[8]]
		}
		
		# increase the nbrs size
		ord <- ord + 1
	} ## while()
	
  # stop workers
	stopCluster(workers)
  
	total_t = proc.time()-start_total
  
	# write results
	cat('Num CI Tests=', n.edgetests, ',Total CI Tests=', sum(unlist(n.edgetests)), ',Total Time=', total_t[3], '\n', sep=" ")
	
	for (i in 1:(p - 1)) {
		for (j in 2:p)
			pMax[i, j] <- pMax[j, i] <- max(pMax[i, j], pMax[j,i])
	}
	
	## transform matrix to graph object :
	Gobject <-
			if (sum(G) == 0) {
				new("graphNEL", nodes = labels)
			} else {
				colnames(G) <- rownames(G) <- labels
				as(G,"graphNEL")
			}
	
	## final object
	new("pcAlgo", graph = Gobject, call = cl, n = integer(0),
			max.ord = as.integer(ord - 1), n.edgetests = n.edgetests,
			sepset = sepset, pMax = pMax, zMin = matrix(NA, 1, 1))
	
}## end{ skeleton }
