#!/bin/tcsh

#PBS -V

### Job name
#PBS -N d0001c4p5000n2000

### Join queuing system output and error files into a single output file
#PBS -j oe

### Send email to user when job ends or aborts
#PBS -m ae

### email address for user
#PBS -M nguyennhattao.hoang@gmail.com

### Queue name that job is submitted to
#PBS -q tizard

### Request nodes, memory, walltime. NB THESE ARE REQUIRED
#PBS -l nodes=1:ppn=4
#PBS -l mem=100gb,vmem=100gb
#PBS -l walltime=22:00:00

# This job's working directory
echo Working directory is $PBS_O_WORKDIR
cd $PBS_O_WORKDIR
echo Running on host `hostname`
echo Time is `date`

# Load module(s) if required
module load gnu/4.4.6 java/java-1.6.0-openjdk R/2.15.3

# Run the executable
R --no-save < runSynTest.R
