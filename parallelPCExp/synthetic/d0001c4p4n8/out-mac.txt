Loading required package: methods
[1] "Dataset:p=4,n=8"
[1] "Creating synthetic dataset..."
[1] "Running Parallel PC..."
Num CI Tests= 6 ,Total CI Tests= 6 ,Total Time= 1.843 
[1] "Cleaning up memory..."
Garbage collection 57 = 41+8+8 (level 2) ... 
31.4 Mbytes of cons cells used (72%)
5.8 Mbytes of vectors used (58%)
         used (Mb) gc trigger (Mb) max used (Mb)
Ncells 587346 31.4     818163 43.7   667722 35.7
Vcells 748884  5.8    1300721 10.0  1288946  9.9
Garbage collection 58 = 41+8+9 (level 2) ... 
31.4 Mbytes of cons cells used (65%)
5.8 Mbytes of vectors used (58%)
         used (Mb) gc trigger (Mb) max used (Mb)
Ncells 587360 31.4     899071 48.1   667722 35.7
Vcells 748912  5.8    1300721 10.0  1288946  9.9
[1] "Running Original PC..."
Num CI Tests= 6 ,Total CI Tests= 6 ,Total Time= 0.001 
[1] "Cleaning up memory..."
Garbage collection 60 = 42+8+10 (level 2) ... 
31.4 Mbytes of cons cells used (60%)
5.8 Mbytes of vectors used (58%)
         used (Mb) gc trigger (Mb) max used (Mb)
Ncells 587812 31.4     984024 52.6   667722 35.7
Vcells 749600  5.8    1300721 10.0  1289207  9.9
[1] "Running Stable PC..."
Num CI Tests= 6 ,Total CI Tests= 6 ,Total Time= 0 
[1] "Cleaning up memory..."
Garbage collection 62 = 43+8+11 (level 2) ... 
31.4 Mbytes of cons cells used (60%)
5.8 Mbytes of vectors used (58%)
         used (Mb) gc trigger (Mb) max used (Mb)
Ncells 587908 31.4     984024 52.6   667722 35.7
Vcells 749668  5.8    1300721 10.0  1289207  9.9
[1] "Parallel vs. Stable"
tpr fpr tdr 
  0   0   1 
[1] "Parallel vs. True"
tpr fpr tdr 
  0   0   1 
[1] "Original vs. True"
tpr fpr tdr 
  0   0   1 
[1] "Cleaning up memory..."
Garbage collection 68 = 48+8+12 (level 2) ... 
31.5 Mbytes of cons cells used (60%)
5.8 Mbytes of vectors used (58%)
         used (Mb) gc trigger (Mb) max used (Mb)
Ncells 589149 31.5     984024 52.6   667722 35.7
Vcells 750356  5.8    1300721 10.0  1299617 10.0
real         3.26
user         1.20
sys          0.08
  68321280  maximum resident set size
         0  average shared memory size
         0  average unshared data size
         0  average unshared stack size
     19607  page reclaims
       832  page faults
         0  swaps
         0  block input operations
         2  block output operations
        33  messages sent
        21  messages received
         1  signals received
       195  voluntary context switches
       335  involuntary context switches
