source("standardPC.R")
source("parallelPC.R")

n=2000
p=4500
d=0.001
  
print(paste("Dataset:","p=",p,",n=",n, sep=""))

# create synthetic dataset
print("Creating synthetic dataset...")
set.seed(50)
rDAG=randomDAG(p, prob=d)
data=rmvDAG(n, rDAG)
suffStat=list(C=cor(data), n=n)
rm(data)
gc()

# run parallel
print("Running Parallel PC...")
parallel <- pc_parallel(suffStat, indepTest=gaussCItest, p=p, skel.method="parallel", alpha=0.01)

# compare parallel & true
print("Parallel vs. True")
print(compareGraphs(parallel@graph, rDAG))

print("Cleaning up memory...")
print(gc(verbose=TRUE))
print(gc(verbose=TRUE))

# run original
print("Running Original PC...")
original <- pc_standard(suffStat, indepTest=gaussCItest, p=p, skel.method="original", alpha=0.01)

# compare original & true
print("Original vs. True")
print(compareGraphs(original@graph, rDAG))

print("Cleaning up memory...")
rm(original)
print(gc(verbose=TRUE))

# run stable
print("Running Stable PC...")
stable <- pc_standard(suffStat, indepTest=gaussCItest, p=p, skel.method="stable", alpha=0.01)

# compare parallel & stable
print("Parallel vs. Stable")
print(compareGraphs(parallel@graph, stable@graph))

# clean up memory
print("Cleaning up memory...")
rm(rDAG)
rm(suffStat)
rm(stable)
rm(parallel)
print(gc(verbose=TRUE))
