#!/bin/bash

for p in 1000 1500 2000 2500 3000 3500 4000 4500 5000
#for p in 4
do
  test_dir="d0001c4p"$p"n2000"
  cd "synthetic/$test_dir/"
  /usr/bin/time -v -o mem-s3.txt Rscript runTest.R > out-s3.txt
  sysctl -w vm.drop_caches=3
  sync && echo 3 | sudo tee /proc/sys/vm/drop_caches
  cd ../..
done


