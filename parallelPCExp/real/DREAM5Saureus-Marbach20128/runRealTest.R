source("standardPC.R")
source("parallelPC.R")

dName <- "DREAM5-Saureus-Marbach2012.csv"

print(paste("Dataset:", dName, sep=""))

# load dataset
print("Loading dataset...")
data <- read.table(dName, header=TRUE, sep=",")
data[1] <- NULL
p <- ncol(data)
n <- nrow(data)
suffStat <- list(C = cor(data), n = n)
rm(data)
gc()

print(paste("n=", n, ", p=", p, sep=""))

# run parallel
print("Running Parallel PC...")
parallel <- pc_parallel(suffStat, indepTest=gaussCItest, p=p, skel.method="parallel", alpha=0.01)

print("Cleaning up memory...")
print(gc(verbose=TRUE))
print(gc(verbose=TRUE))

# run original
print("Running Original PC...")
original <- pc_standard(suffStat, indepTest=gaussCItest, p=p, skel.method="original", alpha=0.01)

# compare parallel & Original
print("Parallel vs. Original")
print(compareGraphs(parallel@graph, original@graph))

print("Cleaning up memory...")
rm(original)
print(gc(verbose=TRUE))

# run stable
print("Running Stable PC...")
stable <- pc_standard(suffStat, indepTest=gaussCItest, p=p, skel.method="stable", alpha=0.01)

# compare parallel & stable
print("Parallel vs. Stable")
print(compareGraphs(parallel@graph, stable@graph))

# clean up memory
print("Cleaning up memory...")
rm(suffStat)
rm(stable)
rm(parallel)
print(gc(verbose=TRUE))