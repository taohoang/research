#!/bin/bash

# function to generate directory for a real dataset
# name=$1, c=$2, mem=$3, time=$4, queue=$5

gen() {
  test_name="${1/-/}$2"

  mkdir "real/$test_name"
  mv "real/$1.csv" "real/$test_name/"
  
  cp template/parallelPC.R "real/$test_name/"
  cp template/standardPC.R "real/$test_name/"
  cp template/runRealTest.R "real/$test_name/"
  cp template/submitTest.txt "real/$test_name/"
  
  sed -i -e "s/numberOfCores/$2/g" "real/$test_name/parallelPC.R"
  sed -i -e "s/numberOfCores/$2/g" "real/$test_name/submitTest.txt"
  sed -i -e "s/datasetName/$1/g" "real/$test_name/runRealTest.R"
  sed -i -e "s/maxMemory/$3/g" "real/$test_name/submitTest.txt"
  sed -i -e "s/wallTime/$4/g" "real/$test_name/submitTest.txt"
  sed -i -e "s/queueName/$5/g" "real/$test_name/submitTest.txt" 
  sed -i -e "s/jobName/$test_name/g" "real/$test_name/submitTest.txt"
  sed -i -e "s/runTest/runRealTest/g" "real/$test_name/submitTest.txt"
}

# generate a test
gen $1 $2 $3 $4 $5

