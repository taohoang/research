#!/bin/bash

# function to submit the test
# d=$1, c=$2, p=$3, n=$4
sub() {
  test_dir="d$1c$2p$3n$4"
  test_dir=${test_dir/./}
  cd "synthetic/$test_dir/"
  qsub submitTest.txt > ID.txt
}

sub $1 $2 $3 $4

