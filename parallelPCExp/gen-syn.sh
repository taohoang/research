#!/bin/bash

# function to generate a test
# d=$1, c=$2, p=$3, n=$4, mem=$5, time=$6, queue=$7
gen() {
  test_dir="d$1c$2p$3n$4"
  test_dir=${test_dir/./}
  mkdir "synthetic/$test_dir"
  cp template/parallelPC.R "synthetic/$test_dir/"
  cp template/standardPC.R "synthetic/$test_dir/"
  cp template/runSynTest.R "synthetic/$test_dir/"
  cp template/submitTest.txt "synthetic/$test_dir/"
  
  sed -i -e "s/numberOfCores/$2/g" "synthetic/$test_dir/parallelPC.R"
  sed -i -e "s/numberOfCores/$2/g" "synthetic/$test_dir/submitTest.txt"
  sed -i -e "s/graphDensity/$1/g" "synthetic/$test_dir/runSynTest.R"
  sed -i -e "s/numberOfVariables/$3/g" "synthetic/$test_dir/runSynTest.R"
  sed -i -e "s/numberOfSamples/$4/g" "synthetic/$test_dir/runSynTest.R"
  sed -i -e "s/maxMemory/$5/g" "synthetic/$test_dir/submitTest.txt"
  sed -i -e "s/wallTime/$6/g" "synthetic/$test_dir/submitTest.txt"
  sed -i -e "s/queueName/$7/g" "synthetic/$test_dir/submitTest.txt" 
  sed -i -e "s/jobName/$test_dir/g" "synthetic/$test_dir/submitTest.txt"
  sed -i -e "s/runTest/runSynTest/g" "synthetic/$test_dir/submitTest.txt"
}

# generate a test
gen $1 $2 $3 $4 $5 $6 $7

