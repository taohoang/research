#!/bin/bash

# function to submit the test
# name=$1, c=$2
sub() {
  test_name="${1/-/}$2"
  cd "real/$test_name/"
  qsub submitTest.txt > ID.txt
}

sub $1 $2
