#!/bin/bash

if [ 1 -eq 0 ]; then
./gensub-syn.sh 0.001 8 1000 2000 16 03:00:00 tizard
./gensub-syn.sh 0.001 8 1500 2000 20 04:00:00 tizard
./gensub-syn.sh 0.001 8 4500 2000 80 18:00:00 tizard
./gensub-syn.sh 0.001 8 2000 2000 32 07:30:00 tizard
./gensub-syn.sh 0.001 8 5000 2000 100 22:00:00 tizard
./gensub-syn.sh 0.001 8 2500 2000 36 08:00:00 tizard
./gensub-syn.sh 0.001 8 3000 2000 48 12:00:00 tizard
./gensub-syn.sh 0.001 8 3500 2000 60 14:00:00 tizard
./gensub-syn.sh 0.001 8 4000 2000 70 16:00:00 tizard
fi

if [ 1 -eq 0 ]; then
./gen-syn.sh 0.001 4 1000 2000 16 03:00:00 tizard
./gen-syn.sh 0.001 4 1500 2000 20 04:00:00 tizard
./gen-syn.sh 0.001 4 4500 2000 80 18:00:00 tizard
./gen-syn.sh 0.001 4 2000 2000 32 07:30:00 tizard
./gen-syn.sh 0.001 4 5000 2000 100 22:00:00 tizard
./gen-syn.sh 0.001 4 2500 2000 36 08:00:00 tizard
./gen-syn.sh 0.001 4 3000 2000 48 12:00:00 tizard
./gen-syn.sh 0.001 4 3500 2000 60 14:00:00 tizard
./gen-syn.sh 0.001 4 4000 2000 70 16:00:00 tizard
fi

./gen-syn.sh 0.001 4 3500 2000 60 14:00:00 tizard
./gen-syn.sh 0.001 4 4000 2000 70 16:00:00 tizard
./gen-syn.sh 0.001 4 4500 2000 80 18:00:00 tizard
./gen-syn.sh 0.001 4 5000 2000 100 22:00:00 tizard

if [ 1 -eq 0 ]; then
./gensub-syn.sh 0.001 16 1000 2000 28 02:00:00 tizard
./gensub-syn.sh 0.001 16 1500 2000 36 03:00:00 tizard
./gensub-syn.sh 0.001 16 4500 2000 100 12:00:00 tizard
./gensub-syn.sh 0.001 16 2000 2000 60 05:00:00 tizard
./gensub-syn.sh 0.001 16 5000 2000 120 17:00:00 tizard
./gensub-syn.sh 0.001 16 2500 2000 68 07:00:00 tizard
./gensub-syn.sh 0.001 16 3000 2000 70 9:00:00 tizard
./gensub-syn.sh 0.001 16 3500 2000 80 10:30:00 tizard
./gensub-syn.sh 0.001 16 4000 2000 90 11:00:00 tizard
fi

