MICrank=function(datacsv, geneNames, miRindex){	
#test=MIC("NCI60.csv", "NCIgenenames.Rdata", 7, "OverlapDatasetExperiment.Rdata", "grountruthmiR200b.Rdata", 1, 20)
	source("MINE.R")
	MINE(datacsv, "master.variable", miRindex)
	filename=paste(datacsv, ",mv=", miRindex, ",cv=0.0,B=n^0.6,Results.csv", sep="")
	result=read.csv(filename, header=TRUE, sep=",")
	result=result[,-1]
	
	load(geneNames)
	result=merge(result, genenames, by.x="Y.var", by.y="Probes")
	result=na.omit(result)
	cat("numberof genes in result:", nrow(result), "\n")
	result=result[order(-result[,2]),]
	result=result[!duplicated(result[,8]),]
	cat("number of genes in the ranking list after removing dulicates", nrow(result), "\n")
	result
}
