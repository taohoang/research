================================READ ME================================================
Required packages: pcalg, multicore, WGCNA, rjava. 

========================================================================================
I. Inferring miRNA-mRNA causal relationships with IDA

We firstly run mIDA function to estimate the causal effects of each miRNA on the mRNAs, and then use the mIDArank function to rank all the possible target genes of a miRNA of interest. 

1. Function: mIDA(dataset, nomirs, outfile)

	�Parameters: �dataset� is the name of the dataset in .csv format; �nomirs� is the number of miRNAs in the dataset; �outfile� is the name of the output file.

	�Example:  In the current directory (/Codes)
		source(�RunmodifiedIDA.R�)
		mIDA("NCI60.csv", 43, "NCIRobustmulti001.RData")

	�Notes: To use parallel computing to reduce the runtime, we should run the program in Linux or Unix systems. To turn off this feature, comment out the first line in the RunmodifiedIDA.R file (i.e. library(multicore)), and delete  �mc� in the mclapply function inside the IDA permutBootstrap function.



2. Function: mIDArank(result, geneNames, miRindex)

	�Parameters: �result� is the output of the mIDA function; �geneNames� is the gene symbols of the mRNAs in the dataset (i.e. NCIgenenames.Rdata ); �miRindex� is the index of the miRNA in the dataset, i.e. the column index of the miRNA in the dataset (not include the sample column).

	�Example: In the current directory (/Codes)
		source(�mIDArank.R�)
		rankResults=mIDArank(�NCIRobustmulti001.RData�,  �NCIgenenames.Rdata�, 7)

	�Notes: miRindex=7 in the EMT dataset corresponds to probe 9578 of miR-200b.

================================================================================================

II. Inferring miRNA-mRNA regulatory relationships with MIC

We need to make sure the MINE.jar and MINE.R files are in the working directory and the rjava package has been installed. We can then use the MICrank function to produce the ranking of possible target mRNAs of a miRNA of interest. 

1. Function: MICrank(datacsv, geneNames, miRindex)

	�Parameters: �datacsv� is the name of the dataset in .csv format; �geneNames� is the file with the gene symbols of mRNAs in the dataset (i.e. NCIgenenames.Rdata ); �miRindex� is the index of the miRNA in the dataset, i.e. the column index of the miRNA in the dataset (not include the sample column).

	�Example: In the current directory (/Codes)
	source(�MICrank.R�)
	rankResults=MICrank(�NCI60.csv�, �NCIgenenames.Rdata�, 7)


=================================================================================================


III. Inferring miRNA-mRNA regulatory relationships with causalMIC

Function causalMICBorda will return the ranking from the causalMIC method. 

Function: causalMICBorda(IDAresult, datacsv, geneNames, miRindex)

	�Parameters: �IDAresult� is the output of the mIDA function; �datacsv� is the dataset 	in .csv format; �geneNames� is the file with the gene symbols of mRNAs in the dataset (i.e. NCIgenenames.Rdata); �miRindex� is the index of the miRNA in the dataset, i.e. the column index of the miRNA in the dataset (not include the sample column).

	�Example: In the current directory (/Codes)
		source(�causalMICBorda.R�)
		finalResults=causalMCIBorda(�NCIRobustmulti001.RData�, �NCI60.csv�, �NCIgenenames.Rdata�, 7)
