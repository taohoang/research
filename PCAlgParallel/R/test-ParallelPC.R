# #!/usr/bin/env Rscript
# ## Test parallelized PC ##
# 
# ## load necessary code
# #source("R/PC-Standard.R")
# source("R/PC-Parallel.R")
# #library(pcalg)
# 
# ## dataset
# rDAG <- randomDAG(180, prob=0.3, lB=0.1, uB=1)
# data <- rmvDAG(100, rDAG, errDist="normal")
# suffStat <- list(C=cor(data), n=nrow(data))
# 
# ## run algorithm
# parallel <- pc_parallel_fast(suffStat, indepTest=gaussCItest, p=ncol(data), skel.method="parallel", alpha=0.01)
# parallel.fast <- pc_parallel_fast(suffStat, indepTest=gaussCItest, p=ncol(data), skel.method="parallel.fast", alpha=0.01)
# #stable.fast <- pc_standard(suffStat, indepTest=gaussCItest, p=ncol(data), skel.method="stable.fast", alpha=0.01)
# 
# ## Compare results
# compareGraphs(parallel.fast@graph, parallel@graph)
# 
# ## print results
# pdag <- as(parallel.fast@graph,"matrix")
# ind <- which((pdag==1 & t(pdag)==1), arr.ind=TRUE)
# undir_edges <- nrow(subset(ind, ind[, 1] < ind[, 2]))
# dir_edges <- nrow(which((pdag==1 & t(pdag)==0), arr.ind=TRUE))
# total_edges <- undir_edges + dir_edges
# cat('Number of undirected edges:', undir_edges, ',',
#     'Number of directed edges:', dir_edges, ',',
#     'Total number of edges:', total_edges, '\n')
# 
# 
# 
# 
# ## result archive
# ## parallel.fast: Num CI Tests= 190 1694 62 ,Total CI Tests= 1946 ,Total Time= 0.056
# ## stable.fast: Num CI Tests= 190 1694 62 ,Total CI Tests= 1946 ,Total Time= 0.191
# ## p = 150: Num CI Tests= 11175 598730 707 8 ,Total CI Tests= 610620 ,Total Time= 0.247 (parallel-fast)
# ## p = 150: Num CI Tests= 11175 598730 707 8 ,Total CI Tests= 610620 ,Total Time= 0.361 (stable-fast)
# ## p = 180: Num CI Tests= 31477 993122 624 24 ,Total CI Tests= 1025247 ,Total Time= 24.669 (parallel)
# ## p = 180: Num CI Tests= 16110 1017324 4652 7393 13174 17513 17574 12048 1575 340 33 ,Total CI Tests= 1107736 ,Total Time= 3.61 (stable.fast)
# ## p = 180: Num CI Tests= 16110 1017324 4652 7393 13174 17513 17574 12048 1575 340 33 ,Total CI Tests= 1107736 ,Total Time= 3.348 (parallel.fast)
# ## p = 180: Num CI Tests= 16110 998185 539 ,Total CI Tests= 1014834 ,Total Time= 0.447 (parallel.fast)
# ## p = 200: Num CI Tests= 39558 1430313 1091 47 5 ,Total CI Tests= 1471014 ,Total Time= 34.08 (parallel)
# 
