pdf("ICDM-Exp-BR51-92.pdf")
# plot Time vs. Number of variables
data1 <- read.csv("ICDM-Exp-BR51-92.csv")
plot(data1$parallel, type="b", lwd=1.5, pch=19, xaxt="n", ylim=c(0,80), col="red", xlab="Number of cores",ylab="Time (min)")
axis(1,at=1:length(data1$NumCores),labels=data1$NumCores)
lines(data1$original,col="blue",type="b",lwd=1.5, pch=23)
lines(data1$stable,col="orange",type="b",lwd=1.5, pch=25)

legend("topleft",legend=c("parallel","original","stable"),
       lty=1,lwd=1.5,pch=c(19,23,25),col=c("red","blue","orange"),
       ncol=1,bty="n",cex=0.8,
       text.col=c("red","blue","orange"),
       inset=0.01)

dev.off()