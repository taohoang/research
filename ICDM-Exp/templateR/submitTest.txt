#!/bin/tcsh

#PBS -V

### Job name
#PBS -N jobName

### Join queuing system output and error files into a single output file
#PBS -j oe

### Send email to user when job ends or aborts
#PBS -m ae

### email address for user
#PBS -M nguyennhattao.hoang@gmail.com

### Queue name that job is submitted to
#PBS -q queueName

### Request nodes, memory, walltime. NB THESE ARE REQUIRED
#PBS -l nodes=1:ppn=numberOfCores
#PBS -l mem=maxMemorygb,vmem=maxMemorygb
#PBS -l walltime=wallTime

# This job's working directory
echo Working directory is $PBS_O_WORKDIR
cd $PBS_O_WORKDIR
echo Running on host `hostname`
echo Time is `date`

# Load module(s) if required
module load gnu/4.4.6 java/java-1.6.0-openjdk R/2.15.3

# Run the executable
R --no-save < runTest.R
