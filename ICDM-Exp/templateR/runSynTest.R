source("algoTypePC.R")

dName <- "datasetName.csv"

print(paste("Dataset:", dName, sep=""))

# load dataset
print("Loading dataset...")
data <- read.table(paste("../", dName, sep=""), sep=",")
p <- ncol(data)
suffStat <- list(C = cor(data), n = nrow(data))
rm(data)
gc()

# Run PC
print("Running algoType PC...")
pc_algoType(suffStat, indepTest=gaussCItest, p=p, skel.method="algoType", alpha=0.01)