#!/opt/shared/R/2.15.3/bin/Rscript

library(pcalg)

genSynData <- function(n, p, d) {
  rDAG <- randomDAG(p, prob=d, lB=0.1, uB=1)
  data <- rmvDAG(n=n, rDAG, errDist="normal")
  rm(rDAG)
  data
}

base_dir <- "../data/synthetic/"

N <- c(500, 1000, 1500, 2000, 2500, 3000)
P <- c(2000)
D <- c(0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05)

for (n in N)
  for (p in P)
    for (d in D) {
      data <- genSynData(n, p, d)
      file_name <- paste("p", p, "n", n, "d", d, sep="")
      file_name <- gsub("[.]", "", file_name)
      dir.create(paste(base_dir, file_name, "/", sep=""))
      write.table(data, file=paste(base_dir, file_name, "/", file_name, ".csv", sep=""),
                  sep=",", col.names=FALSE, row.names=FALSE)
    }
