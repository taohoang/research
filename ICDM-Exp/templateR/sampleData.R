data <- read.table("DREAM5-Insilico-Marbach2012.csv", sep=",")
n <- nrow(data)
p <- ncol(data)

# sample row
for (size in c(200, 400, 600, 800)) {
  rows <- sample(1:n, size, replace=FALSE)
  rows <- sort(rows, decreasing=FALSE)
  sdata <- data[rows,]
  write.table(sdata, file=paste("p1643n", size, "d0004", ".csv", sep=""),
              sep=",", col.names=FALSE, row.names=FALSE)
  rm(sdata)
}

# sample column
for (size in c(500, 750, 1000, 1250, 1500)) {
  cols <- sample(1:p, size, replace=FALSE)
  cols <- sort(cols, decreasing=FALSE)
  sdata <- data[, cols]
  write.table(sdata, file=paste("p", size, "n805", "d0004", ".csv", sep=""),
              sep=",", col.names=FALSE, row.names=FALSE)
  rm(sdata)
}