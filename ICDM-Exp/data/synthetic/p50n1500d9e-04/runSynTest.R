source("parallelPC.R")

dName <- "p50n1500d9e-04.csv"

print(paste("Dataset:", dName, sep=""))

# load dataset
print("Loading dataset...")
data <- read.table(dName, sep=",")
p <- ncol(data)
suffStat <- list(C = cor(data), n = nrow(data))
rm(data)
gc()

# Run PC
print("Running algoType PC...")
pc_parallel(suffStat, indepTest=gaussCItest, p=p, skel.method="parallel", alpha=0.01)