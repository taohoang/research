## Standard FCI algorithms

library(pcalg)

rfci_standard <- function(suffStat, indepTest, alpha, labels, p,
                 skel.method = c("stable", "original", "stable.fast"),
                 fixedGaps = NULL, fixedEdges = NULL,
                 NAdelete = TRUE, m.max = Inf, rules = rep(TRUE, 10),
                 conservative = FALSE, maj.rule = FALSE,
                 verbose = FALSE)
{
  ## Purpose: Perform RFCI-Algorithm, i.e., estimate PAG
  ## ----------------------------------------------------------------------
  ## Arguments:
  ## - suffStat, indepTest: info for the tests
  ## - p: number of nodes in the graph
  ## - alpha: Significance level of individual partial correlation tests
  ## - verbose: 0 - no output, 1 - detailed output
  ## - fixedGaps: the adjacency matrix of the graph from which the algorithm
  ##      should start (logical); gaps fixed here are not changed
  ## - fixedEdges: Edges marked here are not changed (logical)
  ## - NAdelete: delete edge if pval=NA (for discrete data)
  ## - m.max: maximal size of conditioning set
  ## - rules: array of length 10 wich contains TRUE or FALSE corrsponding
  ##          to each rule. TRUE means the rule will be applied.
  ##          If rules==FALSE only R0 (minimal pattern) will be used
  ## - conservative: TRUE or FALSE defining if the v-structures after
  ##                 the skeleton have to be oriented conservatively or nor
  ## - maj.rule: TRUE or FALSE variable containing if the majority rule is
  ##             used instead of the normal conservative
  ## - labels: names of the variables or nodes
  
  ## ----------------------------------------------------------------------
  ## Author: Diego Colombo, 2011; modifications: Martin Maechler
  
  cl <- match.call()
  if(!missing(p)) stopifnot(is.numeric(p), length(p <- as.integer(p)) == 1, p >= 2)
  if(missing(labels)) {
    if(missing(p)) stop("need to specify 'labels' or 'p'")
    labels <- as.character(seq_len(p))
  } else { ## use labels ==> p  from it
    stopifnot(is.character(labels))
    if(missing(p)) {
      p <- length(labels)
    } else if(p != length(labels))
      stop("'p' is not needed when 'labels' is specified, and must match length(labels)")
    else
      message("No need to specify 'p', when 'labels' is given")
  }
  seq_p <- seq_len(p)
  
  if (conservative && maj.rule)
    stop("Can only choose one of conservative or majority rule RFCI")
  if (verbose) cat("Compute Skeleton\n================\n")
  
  skel <- skeleton_standard(suffStat, indepTest, alpha, labels=labels, method = skel.method,
                   fixedGaps=fixedGaps, fixedEdges=fixedEdges,
                   NAdelete=NAdelete, m.max=m.max, verbose=verbose)
  sk.A <- as(skel@graph, "matrix")
  sepset <- skel@sepset
  ##the list of all ordered unshielded triples (the graph g does not change it is just a search!)
  u.t <- find.unsh.triple(sk.A, check=FALSE)
  
  ## check and orient v-structures recursively
  r.v. <- rfci.vStruc_standard(suffStat, indepTest, alpha, sepset, sk.A,
                      unshTripl = u.t$unshTripl, unshVect = u.t$unshVect,
                      conservative = (conservative || maj.rule),
                      version.unf=c(1,1), maj.rule=maj.rule, verbose=verbose)
  A <- r.v.$amat
  sepset <- r.v.$sepset
  
  ## orient as many edge marks as possible
  if (verbose)
    cat("\nDirect egdes:\n=============\nUsing rules:", which(rules), "\n")
  
  res <- udag2apag(A, suffStat, indepTest, alpha, sepset,
                   rules=rules, unfVect = r.v.$unfTripl, verbose=verbose)
  Amat <- res$graph
  colnames(Amat) <- rownames(Amat) <- labels
  new("fciAlgo", amat = Amat, call = cl, n = integer(0),
      max.ord = as.integer(skel@max.ord), max.ordPDSEP = 0L,
      n.edgetests = skel@n.edgetests, n.edgetestsPDSEP = 0,
      sepset = res$sepset, pMax = skel@pMax, allPdsep = vector("list", p))
} ## {rfci}


skeleton_standard <- function(suffStat, indepTest, alpha, labels, p,
                              method = c("stable", "original", "stable.fast"), m.max = Inf,
                              fixedGaps = NULL, fixedEdges = NULL,
                              NAdelete = TRUE, verbose = FALSE)
{ 
  cl <- match.call()
  if(!missing(p)) stopifnot(is.numeric(p), length(p <- as.integer(p)) == 1, p >= 2)
  if(missing(labels)) {
    if(missing(p)) stop("need to specify 'labels' or 'p'")
    labels <- as.character(seq_len(p))
  } else { ## use labels ==> p  from it
    stopifnot(is.character(labels))
    if(missing(p)) {
      p <- length(labels)
    } else if(p != length(labels))
      stop("'p' is not needed when 'labels' is specified, and must match length(labels)")
    else
      message("No need to specify 'p', when 'labels' is given")
  }
  seq_p <- seq_len(p)
  method <- match.arg(method)
  ## C++ version still has problems under Windows; will have to check why
  #  if (method == "stable.fast" && .Platform$OS.type == "windows") {
  #    method <- "stable"
  #    warning("Method 'stable.fast' is not available under Windows; using 'stable' instead.")
  #  }
  
  ## G := !fixedGaps, i.e. G[i,j] is true  iff  i--j  will be investigated
  if (is.null(fixedGaps)) {
    G <- matrix(TRUE, nrow = p, ncol = p)
    diag(G) <- FALSE
  }
  else if (!identical(dim(fixedGaps), c(p, p)))
    stop("Dimensions of the dataset and fixedGaps do not agree.")
  else if (!identical(fixedGaps, t(fixedGaps)) )
    stop("fixedGaps must be symmetric")
  else
    G <- !fixedGaps
  
  if (any(is.null(fixedEdges))) { ## MM: could be sparse
    fixedEdges <- matrix(rep(FALSE, p * p), nrow = p, ncol = p)
  }
  else if (!identical(dim(fixedEdges), c(p, p)))
    stop("Dimensions of the dataset and fixedEdges do not agree.")
  else if (fixedEdges != t(fixedEdges))
    stop("fixedEdges must be symmetric")
  
  start_total <- proc.time()
  
  if (method == "stable.fast") {
    ## Do calculation in C++...
    if (identical(indepTest, gaussCItest))
      indepTestName <- "gauss"
    else
      indepTestName <- "rfun"
    options <- list(
      verbose = as.integer(verbose), 
      m.max = as.integer(ifelse(is.infinite(m.max), p, m.max)),
      NAdelete = NAdelete)
    res <- .Call("estimateSkeleton", G, suffStat, indepTestName, indepTest, alpha, fixedEdges, options);
    G <- res$amat
    # sepset <- res$sepset
    sepset <- lapply(seq_p, function(i) c(
      lapply(res$sepset[[i]], function(v) if(identical(v, as.integer(-1))) NULL else v),
      vector("list", p - length(res$sepset[[i]])))) # TODO change convention: make sepset triangular
    pMax <- res$pMax
    n.edgetests <- res$n.edgetests
    ord <- length(n.edgetests) - 1
  }
  else {
    ## Original R version
    
    pval <- NULL
    sepset <- lapply(seq_p, function(.) vector("list",p))# a list of lists [p x p]
    ## save maximal p value
    pMax <- matrix(-Inf, nrow = p, ncol = p)
    diag(pMax) <- 1
    done <- FALSE
    ord <- 0
    n.edgetests <- numeric(1)# final length = max { ord}
    while (!done && any(G) && ord <= m.max) {
      n.edgetests[ord1 <- ord+1L] <- 0
      done <- TRUE
      ind <- which(G, arr.ind = TRUE)
      ## For comparison with C++ sort according to first row
      ind <- ind[order(ind[, 1]), ]
      remainingEdgeTests <- nrow(ind)
      if (verbose)
        cat("Order=", ord, "; remaining edges:", remainingEdgeTests,"\n",sep="")
      if(method == "stable") {
        ## Order-independent version: Compute the adjacency sets for any vertex
        ## Then don't update when edges are deleted
        G.l <- split(G, gl(p,p))
      }
      for (i in 1:remainingEdgeTests) {
        if (verbose && i%%100 == 0) cat("|i=", i, "|iMax=", nrow(ind), "\n")
        x <- ind[i, 1]
        y <- ind[i, 2]
        if (G[y, x] && !fixedEdges[y, x]) {
          nbrsBool <- if(method == "stable") G.l[[x]] else G[,x]
          nbrsBool[y] <- FALSE
          nbrs <- seq_p[nbrsBool]
          length_nbrs <- length(nbrs)
          if (length_nbrs >= ord) {
            if (length_nbrs > ord)
              done <- FALSE
            S <- seq_len(ord)
            repeat { ## condition w.r.to all  nbrs[S] of size 'ord'
              n.edgetests[ord1] <- n.edgetests[ord1] + 1
              pval <- indepTest(x, y, nbrs[S], suffStat)
              if (verbose)
                cat("x=", x, " y=", y, " S=", nbrs[S], ": pval =", pval, "\n")
              if(is.na(pval))
                pval <- as.numeric(NAdelete) ## = if(NAdelete) 1 else 0
              if (pMax[x, y] < pval)
                pMax[x, y] <- pval
              if(pval >= alpha) { # independent
                G[x, y] <- G[y, x] <- FALSE
                sepset[[x]][[y]] <- nbrs[S]
                break
              }
              else {
                nextSet <- getNextSet(length_nbrs, ord, S)
                if (nextSet$wasLast)
                  break
                S <- nextSet$nextSet
              }
            } ## repeat
          }
        }
      }# for( i )
      ord <- ord + 1
    } ## while()
    for (i in 1:(p - 1)) {
      for (j in 2:p)
        pMax[i, j] <- pMax[j, i] <- max(pMax[i, j], pMax[j,i])
    }
  }
  
  total_t = proc.time()-start_total
  
  # write results
  cat('Num CI Tests=', n.edgetests, ',Total CI Tests=', sum(unlist(n.edgetests)), ',Total Time=', total_t[3], '\n', sep=" ")
  
  ## transform matrix to graph object :
  Gobject <-
    if (sum(G) == 0) {
      new("graphNEL", nodes = labels)
    } else {
      colnames(G) <- rownames(G) <- labels
      as(G,"graphNEL")
    }
  
  ## final object
  new("pcAlgo", graph = Gobject, call = cl, n = integer(0),
      max.ord = as.integer(ord - 1), n.edgetests = n.edgetests,
      sepset = sepset, pMax = pMax, zMin = matrix(NA, 1, 1))
  
}## end{ skeleton }


##' called only from  rfci()  and  checkEdges()
rfci.vStruc_standard <- function(suffStat, indepTest, alpha, sepset, g.amat,
                        unshTripl, unshVect, conservative = FALSE,
                        version.unf=c(2,1), maj.rule = FALSE, verbose = FALSE)
{
  ## Purpose: check the unshielded triples in unshTripl as v-structures
  ##          recursively check if new unshielded triples have been found
  ##          then save and orient the final ones in finalList and finalVect
  ## ----------------------------------------------------------------------
  ## Arguments: - suffStat, indepTest, p ,alpha: arguments from the algorithm
  ##            - sepset, g.amat: output of the function skeleton
  ##            - unshTripl, unshVect: list/numbers of the unshielded triples
  ##                                   in graph
  ##            - conservative: TRUE or FALSE
  ##            - version.unf=c(2,1): conservative version
  ##            - maj.rule: TRUE or FALSE variable containing if the majority
  ##             rule is used instead of the normal conservative
  ## ----------------------------------------------------------------------
  ## Values: - updated sepset, graph (g.amat) and list of unfaithful v-structures
  ## ----------------------------------------------------------------------
  ## Author: Diego Colombo, Date: 21 Oct 2010, 14:15
  
  ## check every column (unshielded triple) of unshTripl
  ## NOTE: what's done is done!!!! I don't look back in the matrix
  
  stopifnot(is.matrix(unshTripl), nrow(unshTripl) == 3)
  nT <- ncol(unshTripl)
  
  ## list of all unfaithful v-structures
  unfTripl <- NULL
  if (nT) {
    if (verbose)
      cat("\nCheck unshielded triples for conditional dependence
          ================================================\n")
    ## check all the columns in unshTripl as v-structure or not
    checktmp <- dep.triple_standard(suffStat, indepTest, alpha, sepset=sepset, apag=g.amat,
                           unshTripl=unshTripl, unshVect=unshVect,
                           ##per default every triple is defined as a v-structure :
                           trueVstruct = rep(TRUE, nT), verbose=verbose)
    ##save the updated objects
    A          <- checktmp$ apag
    sepset     <- checktmp$ sepset
    ## note that no column is deleted from the matrix, we can add new triples instead
    unshTripl  <- checktmp$ triple
    unshVect   <- checktmp$ vect
    trueVstruct<- checktmp$ trueVstruct
    
    if (verbose) cat(
      if (conservative)
        "\nOrient the v-structures conservatively
      =====================================\n"
      else "\nOrient the v-structures\n=======================\n")
    
    ## if there is at least one triple with the desired properties
    if (any(trueVstruct)) {
      
      start_t <- proc.time()
      
      for (i in 1:dim(unshTripl)[2]) {
        if (trueVstruct[i]) {
          x <- unshTripl[1, i]
          y <- unshTripl[2, i]
          z <- unshTripl[3, i]
          if (!conservative) {
            if (A[x, z] == 0 && A[x,y] != 0 && A[z,y] != 0 &&
                  !((y %in% sepset[[x]][[z]]) || (y %in% sepset[[z]][[x]]))) {
              ## this is to avoid the problem of:
              ## assume that <a,b,c> was saved in finalList because a "true" unshielded triple
              ## but after in the matrix appears <b,c,d> and the edge b-c is deleted
              ## of course the triple <a,b,c> stays in the finalList but we cannot orient the edge
              ## b-c because it doesn'e exist anymore
              
              if (verbose)
                cat("\n", x, "*->", y, "<-*", z,
                    "\nSxz=", sepset[[z]][[x]], "and",
                    "Szx=", sepset[[x]][[z]], "\n")
              A[c(x,z), y] <- 2
            }
          } else { ## conservative version
            ## check if x-y-z is faithful
            ## find neighbours of x and z
            nbrsX <- which(A[,x]!=0) ## G symm; x no nbr of z
            nbrsZ <- which(A[,z]!=0)
            if (verbose)
              cat("\nTriple:",x,y,z,"and sepset by skelet:",
                  unique(sepset[[x]][[z]],sepset[[z]][[x]]),"\n")
            r.abc <- checkTriple(x, y, z, nbrsX, nbrsZ,
                                 sepset[[x]][[z]], sepset[[z]][[x]],
                                 suffStat=suffStat, indepTest=indepTest, alpha=alpha,
                                 version.unf=version.unf, maj.rule=maj.rule,
                                 verbose=verbose)
            p <- nrow(A)
            ## 1: in NO set; 2: in ALL sets; 3: in SOME but not all
            if (r.abc$decision == 3 || ## <- take action if case "3", or
                  ## can happen the case in Tetrad, so we must save the triple as unfaithful
                  ## a and c independent given S but not given subsets of the adj(x) or adj(z)
                  (version.unf[1] == 2 && r.abc$version == 2)) {
              
              ## record unfaithful triple
              unfTripl <- c(unfTripl, triple2numb(p, x, y, z))
              ##if (verbose) {
              ##  cat("new unfTriple:", x, y, z, "\n")
              ##}
            }
            sepset[[x]][[z]] <- r.abc$SepsetA
            sepset[[z]][[x]] <- r.abc$SepsetC
            if (A[x,z] == 0 && A[x,y] != 0 && A[z,y] != 0 &&
                  !((y %in% sepset[[x]][[z]]) ||
                      (y %in% sepset[[z]][[x]])) &&
                  !any(unfTripl==triple2numb(p, x, y, z), na.rm=TRUE) &&
                  !any(unfTripl==triple2numb(p, z, y, x), na.rm=TRUE)) {
              if (verbose)
                cat("\nOrient:", x, "*->", y, "<-*", z,
                    "\nSxz=", sepset[[z]][[x]], "and",
                    "Szx=", sepset[[x]][[z]], "\n")
              A[c(x,z), y] <- 2
            }
          } ## else : conservative
        }
      } ## for (i ...)
      
      total_t <- proc.time() - start_t
      
      # write results
      cat("Time of rfci.vStruc_standard() =", total_t[3], '\n', sep=" ")
    }
  }
  list(sepset = sepset, amat = A, unfTripl = unfTripl)
} ## {rfci.vStruc}


## only called from rfci.vStruc() - with trueVstruct a vector of TRUE
dep.triple_standard <- function(suffStat, indepTest, alpha, sepset, apag,
                       unshTripl, unshVect, trueVstruct, verbose = FALSE)
{
  ## Purpose: test the two edges of any unshielded triple in unshTripl (column)
  ##          for dependence given sepset. If independent find the minimal
  ##          sepset, delete the edge, define the triple as FALSE in
  ##          trueVstruct and search in the graph for new triple or destroyed
  ##          ones. Otherwise do nothing.
  ## ----------------------------------------------------------------------
  ## Arguments: - suffStat, indepTest, alpha, sepset, apag: skeleton parameters / result
  ##            - unshTripl: matrix containing the unshielded triples (columns)
  ##            - unshVect: triple2numb of unshTripl
  ##            - trueVstruct: vector containing T/F for the v-structures
  ## ----------------------------------------------------------------------
  ## Values: - updated sepset, apag, unshTripl, unshVect, trueVstruct
  ## Author: Diego Colombo, Date: 16 Aug 2010, 15:07
  
  start_t <- proc.time()
  
  p <- nrow(apag) ## == length(sepset)
  for(k in seq_len(ncol(unshTripl))) {
    ## Note that trueVstruct[.] is changed *inside* !
    if (trueVstruct[k]) { ## triple under inspection x-y-z
      x <- unshTripl[1, k]
      y <- unshTripl[2, k]
      z <- unshTripl[3, k]
      SepSet <- setdiff(unique(c(sepset[[x]][[z]],
                                 sepset[[z]][[x]])), y)
      nSep <- length(SepSet)
      if (verbose)
        cat("\nTriple:", x, y, z, "and sepSet (of size", nSep,") ", SepSet,"\n")
      if (nSep != 0) {
        x. <- min(x,y)
        y. <- max(x,y)
        y_ <- min(y,z)
        z_ <- max(y,z)
        
        ## Check x-y ---------------------------------------------------------------
        
        del1 <- FALSE
        ## check first x and y given the whole sepset(x,z)
        if ((pv1 <- indepTest(x, y, SepSet, suffStat)) >= alpha) {
          ##afterwards we have to define this triple as FALSE in trueVstruct
          del1 <- TRUE
          ## x and y are independent, then find the minimal sepset
          done <- FALSE
          ord <- 0
          while (!done && ord < nSep) {
            ord <- ord + 1
            ## all combinations of SepSet of size ord
            S.j <- if (ord == 1 && nSep == 1) matrix(SepSet,1,1) else combn(SepSet, ord)
            for (i in seq_len(ncol(S.j))) {
              pval <- indepTest(x, y, S.j[,i], suffStat)
              if (verbose) cat("x=", x, " y=", y, "S=", S.j[,i], ": pval =", pval, "\n")
              if (pval >= alpha) {
                ##delete edge and save set in sepset
                apag[x, y] <- apag[y, x] <- 0
                sepset[[x]][[y]] <- sepset[[y]][[x]] <- S.j[,i]
                done <- TRUE
                break
              }
            }
          }
          
          ## case 1: before we had a triangle and now it is an unshielded triple x-m-y
          indM <- which((apag[x, ] == 1 & apag[, x] == 1) & (apag[y, ] == 1 & apag[, y] == 1))
          indM <- setdiff(indM, c(x,y,z)) ##just to be sure
          for (m in indM) {
            ## in the matrix the first column is always smaller than the third
            unshTripl <- cbind(unshTripl,        c(x., m, y.))
            unshVect <- c(unshVect, triple2numb(p, x., m, y.))
            ##per default this new triple is set as TRUE in trueVstruct
            trueVstruct <- c(trueVstruct, TRUE)
          }
          ## case 2: an existent unshielded triple has been destroyed
          ## case 2.a): we had q-x-y or y-x-q in the matrix but not anymore
          indQ <- which((apag[x, ] == 1 & apag[, x] == 1) & (apag[y, ] == 0 & apag[, y] == 0))
          indQ <- setdiff(indQ, c(x,y,z)) ##just to be sure
          for (q in indQ) {
            ## define the triple as FALSE in trueVstruct
            delTripl <- unshVect == (
              if (q < y) triple2numb(p, q, x, y) else triple2numb(p, y, x, q))
            ## if we haven't checked the triple yet, define it as FALSE
            if (any(delTripl))
              trueVstruct[which.max(delTripl)] <- FALSE
          }
          
          ## case 2.b): we had r-y-x or x-y-r in the matrix but not anymore
          indR <- which((apag[x, ] == 0 & apag[, x] == 0) & (apag[y, ] == 1 & apag[, y] == 1))
          indR <- setdiff(indR, c(x,y,z)) ##just to be sure
          ##                ^^^ (had typo here: 'indQ' !!)
          for (r in indR) {
            ##define the triple as FALSE in trueVstruct
            delTripl <- unshVect == (
              if (r < x) triple2numb(p, r, y, x) else triple2numb(p, x, y, r))
            if (any(delTripl))
              trueVstruct[which.max(delTripl)] <- FALSE
          }
          
        } ## if (pv1 .. indepTest(..) )
        
        ## Check z-y ---------------------------------------------------------------
        
        del2 <- FALSE
        ## check first z and y given the whole sepset(x,z)
        if ((pv2 <- indepTest(z, y, SepSet, suffStat)) >= alpha) {
          del2 <- TRUE ## will have to set this triple as FALSE in trueVstruct
          ## z and y are independent, then find the minimal sepset
          Done <- FALSE
          Ord <- 0
          while (!Done && Ord < nSep) {
            Ord <- Ord + 1
            ## all combinations of SepSet of size Ord
            S.j <- if (Ord == 1 && nSep == 1) matrix(SepSet,1,1) else combn(SepSet, Ord)
            for (i in seq_len(ncol(S.j))) {
              pval <- indepTest(z, y, S.j[,i], suffStat)
              if (verbose) cat("x=", z, " y=", y, " S=", S.j[,i], ": pval =", pval, "\n")
              if (pval >= alpha) {
                ##delete edge and save set in sepset
                apag[z, y] <- apag[y, z] <- 0
                sepset[[z]][[y]] <- sepset[[y]][[z]] <- S.j[,i]
                Done <- TRUE
                break
              }
            }
          } ## while( . )
          
          ## case 1: before we had a triangle and now it is an unshielded triple z-m-y
          indM <- which((apag[z, ] == 1 & apag[, z] == 1) & (apag[y, ] == 1 & apag[, y] == 1))
          indM <- setdiff(indM,c(x,y,z)) ##just to be sure
          for (m in indM) {
            ##in the matrix the first column is always smaller than the third
            unshTripl <- cbind(unshTripl,        c(y_, m, z_))
            unshVect <- c(unshVect, triple2numb(p, y_, m, z_))
            ##per default the triple is defined as TRUE
            trueVstruct <- c(trueVstruct,TRUE)
          }
          
          ## case 2: an existent unshielded triple has been destroyed
          ## case 2.a): we had q-z-y or y-z-q in the matrix but not anymore
          indQ <- which((apag[z, ] == 1 & apag[, z] == 1) & (apag[y, ] == 0 & apag[, y] == 0))
          indQ <- setdiff(indQ, c(x,y,z)) ##just to be sure
          for (q in indQ) {
            ## define the triple as FALSE in trueVstruct
            delTripl <- unshVect == (if (q < y) triple2numb(p, q, z, y) else triple2numb(p, y, z, q))
            ## if we haven't checked the triple yet, save it as FALSE
            if (any(delTripl))
              trueVstruct[which.max(delTripl)] <- FALSE
          }
          ## case 2.b): we had r-y-z or z-y-r in the matrix but not anymore
          indR <- which((apag[z, ] == 0 & apag[, z] == 0) & (apag[y, ] == 1 & apag[, y] == 1))
          indR <- setdiff(indR, c(x,y,z)) ##just to be sure
          ##                ^^^ (had typo here: 'indQ' !!)
          for (r in indR) {
            ## define the triple as FALSE in trueVstruct
            delTripl <- unshVect == (
              if (r < z) triple2numb(p, r, y, z) else triple2numb(p, z, y, r))
            if (any(delTripl))
              trueVstruct[which.max(delTripl)] <- FALSE
          }
          
        }
        ## if at least one edge has been deleted this is not a future v-structure
        if (any(del1, del2)) trueVstruct[k] <- FALSE
        
      } ## if (nSep != 0)
      ##
      ## ELSE: the sepset is empty
      ##   so surely <x,y,z> is an unshielded triple because they cannot be indep given the empty set
      ##   nothing changed in sepset and in graph
      ## else{
      ##   sepset <- sepset
      ##   apag <- apag
      ## }
      
      ##recursion on the next column of unshTripl
      ##rec.res <- dep.triple(suffStat, indepTest, p, alpha, sepset, apag, unshTripl, unshVect, trueVstruct, k, verbose=verbose)
      ##save the modified objects
      ##unshTripl <- rec.res$triple
      ##unshVect <- rec.res$vect
      ##sepset <- rec.res$sepset
      ##apag <- rec.res$graph
      ##trueVstruct <- rec.res$trueVstruct
    }
  }## for ( k )
  
  total_t <- proc.time() - start_t
  
  # write results
  cat("Time of dep.triple_standard() =", total_t[3], '\n', sep=" ")
  
  list(triple = unshTripl, vect = unshVect, sepset = sepset,
       apag = apag, trueVstruct = trueVstruct)
} ## {dep.triple}