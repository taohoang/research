library(PCAlgParallel)
source("R/GIES-Standard.R")
source("R/GIES-Parallel.R")

######## Test 1 ###########
## Load predefined data
data(gmInt)
## Define the score (BIC)
score <- new("GaussL0penIntScore", gmInt$x, gmInt$targets, gmInt$target.index)
## Estimate the essential graph
start_t <- proc.time()
gies.fit <- gies(ncol(gmInt$x), gmInt$targets, score)
total_t <- proc.time() - start_t
cat("Total time = ", total_t)

if (require(Rgraphviz)) {
  par(mfrow=c(1,2))
  plot(gies.fit$essgraph, main = "Estimated ess. graph")
  plot(gmInt$g, main = "True DAG")
}


######### Test 2 ##########
data(gmG)
score <- new("GaussL0penObsScore", gmG$x)
ges.fit <- ges(ncol(gmG$x), score)
par(mfrow=1:2); plot(gmG$g, main = ""); plot(ges.fit$essgraph, main = "")

######## Synthetic dataset ########
set.seed(78)
p <- 100
n <- 5000
gGtrue <- randomDAG(p, prob = 0.3)
pardag <- as(gGtrue, "GaussParDAG")
pardag$set.err.var(rep(1, p))
targets <- list(integer(0), 3, 5)
target.index <- c(rep(1, 0.6*n), rep(2, n/5), rep(3, n/5))
x1 <- rmvnorm.ivent(0.6*n, pardag)
x2 <- rmvnorm.ivent(n/5, pardag, targets[[2]],
                    matrix(rnorm(n/5, mean = 4, sd = 0.02), ncol = 1))
x3 <- rmvnorm.ivent(n/5, pardag, targets[[3]],
                    matrix(rnorm(n/5, mean = 4, sd = 0.02), ncol = 1))
gies_data <- list(x = rbind(x1, x2, x3), targets = targets, target.index = target.index, g = gGtrue)

score <- new("GaussL0penIntScore", gies_data$x, gies_data$targets, gies_data$target.index)
## Estimate the essential graph
start_t <- proc.time()
gies.fit <- gies(ncol(gies_data$x), gies_data$targets, score)
total_t <- proc.time() - start_t
cat("Total time = ", total_t)







