#!/usr/bin/env Rscript

library(pryr)

source("Exp1/PC-Standard.R")
source("Exp1/PC-Parallel.R")

######### Experiment 1 ##########
n=10000 # num samples
sink(file="Exp1/Exp1.out")
for (p in seq(1000, 5000, by=500)) { # loop through different num variables
  
  print(paste("Dataset:","p=",p,",n=",n, sep=""))
  
  # create synthetic dataset
  print("Creating synthetic dataset...")
  set.seed(100)
  rDAG=randomDAG(p, prob=0.2)
  data=rmvDAG(n, rDAG)
  suffStat=list(C=cor(data), n=nrow(data))
  
  # run parallel
  print("Running Parallel PC...")
  mem <- mem_change(
  parallel <- pc_parallel(suffStat, indepTest=gaussCItest, p=ncol(data), skel.method="parallel", alpha=0.01)
  )
  print(paste("Memory used=",mem," B",sep=""))
  
  # clean up memory
  print("Cleaning up memory...")
  print(gc(verbose=TRUE))
  
  # sleep for a while
  print("Sleeping for 30s...")
  Sys.sleep(30)
  
  # run original
  print("Running Original PC...")
  mem <- mem_change(
  original <- pc_standard(suffStat, indepTest=gaussCItest, p=ncol(data), skel.method="original", alpha=0.01)
  )
  print(paste("Memory used=",mem," B",sep=""))
  
  print("Cleaning up memory...")
  print(gc(verbose=TRUE))
  
  # run stable
  print("Running Stable PC...")
  mem <- mem_change(
  stable <- pc_standard(suffStat, indepTest=gaussCItest, p=ncol(data), skel.method="stable", alpha=0.01)
  )
  print(paste("Memory used=",mem," B",sep=""))
  
  print("Cleaning up memory...")
  print(gc(verbose=TRUE))
  
  # compare parallel & stable
  print("Parallel vs. Stable")
  print(compareGraphs(parallel@graph, stable@graph))
  
  # compare parallel & true
  print("Parallel vs. True")
  print(compareGraphs(parallel@graph, rDAG))
  
  # compare original & true
  print("Original vs. True")
  print(compareGraphs(original@graph, rDAG))
  
  # clean up memory
  print("Cleaning up memory...")
  rm(rDAG)
  rm(suffStat)
  rm(original)
  rm(stable)
  rm(parallel)
  rm(data)
  print(gc(verbose=TRUE))
  
  # sleep for a while
  print("Sleeping for 30s...")
  Sys.sleep(30)
  
  print("----------------------------------------")
}
sink()

